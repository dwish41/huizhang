/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : huizhang

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2022-03-08 14:04:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_emp_demand_work_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_demand_work_type`;
CREATE TABLE `sys_emp_demand_work_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emp_demand_id` bigint(20) DEFAULT NULL COMMENT 'sys_emp_demand主键id',
  `emp_demand_name` varchar(100) DEFAULT NULL COMMENT '项目名称',
  `work_type_id` bigint(20) DEFAULT NULL COMMENT 'work_type主键id',
  `work_type_name` varchar(100) DEFAULT NULL COMMENT '工种名称',
  `man_hour_fee` double(15,2) DEFAULT NULL COMMENT '工时费',
  `commission` double(15,2) DEFAULT NULL COMMENT '佣金',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 1:禁用，0:正常',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
