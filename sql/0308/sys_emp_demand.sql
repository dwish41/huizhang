/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : huizhang

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2022-03-08 14:04:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_emp_demand
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_demand`;
CREATE TABLE `sys_emp_demand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '项目名称',
  `address` varchar(1000) DEFAULT NULL COMMENT '项目地址',
  `attach_id` varchar(100) DEFAULT NULL COMMENT '项目描述图片',
  `remark` text COMMENT '项目描述',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 1:禁用，0:正常',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
