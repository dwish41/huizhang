/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : huizhang

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2022-03-03 23:42:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '基础管理', '', '', '0', 'fa fa-bars', '1', '2017-08-09 22:49:47', null);
INSERT INTO `sys_menu` VALUES ('2', '3', '系统菜单', 'sys/menu/', 'sys:menu:menu', '1', 'fa fa-th-list', '2', '2017-08-09 22:55:15', null);
INSERT INTO `sys_menu` VALUES ('3', '0', '系统管理', '', '', '0', 'fa fa-desktop', '3', '2017-08-09 23:06:55', '2017-08-14 14:13:43');
INSERT INTO `sys_menu` VALUES ('6', '3', '用户管理', 'sys/user/', 'sys:user:user', '1', 'fa fa-user', '0', '2017-08-10 14:12:11', null);
INSERT INTO `sys_menu` VALUES ('7', '3', '角色管理', 'sys/role', 'sys:role:role', '1', 'fa fa-paw', '1', '2017-08-10 14:13:19', null);
INSERT INTO `sys_menu` VALUES ('12', '6', '新增', '', 'sys:user:add', '2', '', '0', '2017-08-14 10:51:35', null);
INSERT INTO `sys_menu` VALUES ('13', '6', '编辑', '', 'sys:user:edit', '2', '', '0', '2017-08-14 10:52:06', null);
INSERT INTO `sys_menu` VALUES ('14', '6', '删除', null, 'sys:user:remove', '2', null, '0', '2017-08-14 10:52:24', null);
INSERT INTO `sys_menu` VALUES ('15', '7', '新增', '', 'sys:role:add', '2', '', '0', '2017-08-14 10:56:37', null);
INSERT INTO `sys_menu` VALUES ('20', '2', '新增', '', 'sys:menu:add', '2', '', '0', '2017-08-14 10:59:32', null);
INSERT INTO `sys_menu` VALUES ('21', '2', '编辑', '', 'sys:menu:edit', '2', '', '0', '2017-08-14 10:59:56', null);
INSERT INTO `sys_menu` VALUES ('22', '2', '删除', '', 'sys:menu:remove', '2', '', '0', '2017-08-14 11:00:26', null);
INSERT INTO `sys_menu` VALUES ('24', '6', '批量删除', '', 'sys:user:batchRemove', '2', '', '0', '2017-08-14 17:27:18', null);
INSERT INTO `sys_menu` VALUES ('25', '6', '停用', null, 'sys:user:disable', '2', null, '0', '2017-08-14 17:27:43', null);
INSERT INTO `sys_menu` VALUES ('26', '6', '重置密码', '', 'sys:user:resetPwd', '2', '', '0', '2017-08-14 17:28:34', null);
INSERT INTO `sys_menu` VALUES ('27', '91', '系统日志', 'common/log', 'common:log', '1', 'fa fa-warning', '0', '2017-08-14 22:11:53', null);
INSERT INTO `sys_menu` VALUES ('28', '27', '刷新', null, 'sys:log:list', '2', null, '0', '2017-08-14 22:30:22', null);
INSERT INTO `sys_menu` VALUES ('29', '27', '删除', null, 'sys:log:remove', '2', null, '0', '2017-08-14 22:30:43', null);
INSERT INTO `sys_menu` VALUES ('30', '27', '清空', null, 'sys:log:clear', '2', null, '0', '2017-08-14 22:31:02', null);
INSERT INTO `sys_menu` VALUES ('48', '77', '代码生成', 'common/generator', 'common:generator', '1', 'fa fa-code', '3', null, null);
INSERT INTO `sys_menu` VALUES ('49', '0', '内容管理', '', '', '0', 'fa fa-edit', '2', null, null);
INSERT INTO `sys_menu` VALUES ('55', '7', '编辑', '', 'sys:role:edit', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('56', '7', '删除', '', 'sys:role:remove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('57', '91', '运行监控', '/druid/index.html', '', '1', 'fa fa-caret-square-o-right', '1', null, null);
INSERT INTO `sys_menu` VALUES ('61', '2', '批量删除', '', 'sys:menu:batchRemove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('62', '7', '批量删除', '', 'sys:role:batchRemove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('72', '77', '计划任务', 'common/job', 'common:taskScheduleJob', '1', 'fa fa-hourglass-1', '4', null, null);
INSERT INTO `sys_menu` VALUES ('73', '3', '部门管理', '/system/sysDept', 'system:sysDept:sysDept', '1', 'fa fa-users', '3', null, null);
INSERT INTO `sys_menu` VALUES ('74', '73', '增加', '/system/sysDept/add', 'system:sysDept:add', '2', null, '1', null, null);
INSERT INTO `sys_menu` VALUES ('75', '73', '刪除', 'system/sysDept/remove', 'system:sysDept:remove', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('76', '73', '编辑', '/system/sysDept/edit', 'system:sysDept:edit', '2', null, '3', null, null);
INSERT INTO `sys_menu` VALUES ('77', '0', '系统工具', '', '', '0', 'fa fa-gear', '4', null, null);
INSERT INTO `sys_menu` VALUES ('78', '1', '数据字典', '/common/dict', 'common:dict:dict', '1', 'fa fa-book', '6', null, null);
INSERT INTO `sys_menu` VALUES ('79', '78', '增加', '/common/dict/add', 'common:dict:add', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('80', '78', '编辑', '/common/dict/edit', 'common:dict:edit', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('81', '78', '删除', '/common/dict/remove', 'common:dict:remove', '2', '', '3', null, null);
INSERT INTO `sys_menu` VALUES ('83', '78', '批量删除', '/common/dict/batchRemove', 'common:dict:batchRemove', '2', '', '4', null, null);
INSERT INTO `sys_menu` VALUES ('91', '0', '系统监控', '', '', '0', 'fa fa-video-camera', '5', null, null);
INSERT INTO `sys_menu` VALUES ('92', '91', '在线用户', 'sys/online', '', '1', 'fa fa-user', null, null, null);
INSERT INTO `sys_menu` VALUES ('111', '1', '轮播图管理', '/system/advertisement', 'system:advertisement:advertisement', '1', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('112', '111', '新增', '/system/advertisement/add', 'system:advertisement:add', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('113', '111', '修改', '/system/advertisement/update', 'system:advertisement:edit', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('114', '111', '删除', '/system/advertisement/remove', 'system:advertisement:remove', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('115', '111', '批量删除', '/system/advertisement/batchRemove', 'system:advertisement:batchRemove', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('121', '1', '友情链接管理', '/system/link', 'system:link:link', '1', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('122', '121', '新增', '/system/link/add', 'system:link:add', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('123', '121', '修改', '/system/link/edit', 'system:link:edit', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('124', '121', '删除', '/system/link/remove', 'system:link:remove', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('125', '121', '批量删除', '/system/link/batchRemove', 'system:link:batchRemove', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('126', '1', '参数配置', '/sys/sysSet/sysSet', 'sys:sysSet:sysSet', '1', '', '5', null, null);
INSERT INTO `sys_menu` VALUES ('127', '49', '新闻中心', '', 'scenery:article', '1', '', '1', null, null);
INSERT INTO `sys_menu` VALUES ('150', '127', '新闻列表', '/scenery/article/article/zxgg', 'scenery:article', '1', '', '2', null, null);
INSERT INTO `sys_menu` VALUES ('151', '127', '注意事项', '/scenery/article/article/zysx', 'scenery:article', '1', '', '3', null, null);
INSERT INTO `sys_menu` VALUES ('152', '49', '关于我们', '', 'scenery:article:gywm', '1', '', '2', null, null);
INSERT INTO `sys_menu` VALUES ('153', '152', '公司介绍', '/scenery/article/gywm/gsjs', 'scenery:article:gywm', '1', '', '1', null, null);
INSERT INTO `sys_menu` VALUES ('155', '49', '通知公告', '/oa/notify', 'oa:notify:notify', '1', '', '2', null, null);
INSERT INTO `sys_menu` VALUES ('156', '49', '留言管理', '/system/word', 'system:word:word', '1', '', '4', null, null);
INSERT INTO `sys_menu` VALUES ('157', '1', '企业管理', '/system/company', 'system:company:company', '1', '', '1', null, null);
INSERT INTO `sys_menu` VALUES ('158', '1', '代理管理', '/sys/agent', 'sys:agent:agent', '1', '', '2', null, null);
INSERT INTO `sys_menu` VALUES ('159', '1', '员工管理', '/sys/staff', 'sys:staff:staff', '1', '', '3', null, null);
INSERT INTO `sys_menu` VALUES ('160', '1', '地区管理', '/common/area/', 'common:area:list', '1', '', '4', null, null);
INSERT INTO `sys_menu` VALUES ('161', '0', '用工管理', '', '', '0', 'fa fa-group', '6', null, null);
INSERT INTO `sys_menu` VALUES ('162', '161', '工种管理', 'emp/workType/', 'emp:workType:workType', '1', '', '0', null, null);
INSERT INTO `sys_menu` VALUES ('163', '162', '新增', '', 'emp:workType:add', '2', '', '0', null, null);
INSERT INTO `sys_menu` VALUES ('164', '162', '编辑', '', 'emp:workType:edit', '2', '', '1', null, null);
INSERT INTO `sys_menu` VALUES ('165', '162', '删除', '', 'emp:workType:remove', '2', '', '3', null, null);
INSERT INTO `sys_menu` VALUES ('166', '162', '批量删除', '', 'emp:workType:batchRemove', '2', '', '4', null, null);
INSERT INTO `sys_menu` VALUES ('167', '162', '停用', '', 'emp:workType:disable', '2', '', '5', null, null);
