/*
SQLyog Ultimate v11.25 (64 bit)
MySQL - 5.7.25 : Database - huizhang
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`huizhang` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `huizhang`;

/*Table structure for table `sys_work_type` */

DROP TABLE IF EXISTS `sys_work_type`;

CREATE TABLE `sys_work_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '工种名',
  `man_hour_fee` double(15,2) NOT NULL COMMENT '工时费',
  `commission` double(15,2) NOT NULL COMMENT '佣金',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 1:禁用，0:正常',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_work_type` */

insert  into `sys_work_type`(`id`,`name`,`man_hour_fee`,`commission`,`status`,`user_id_create`,`gmt_create`,`gmt_modified`) values (7,'高级JAVA工程师',70.00,30.00,1,NULL,NULL,NULL),(8,'中级JAVA工程师',50.00,20.00,1,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
