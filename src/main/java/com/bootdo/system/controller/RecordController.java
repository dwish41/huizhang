package com.bootdo.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.system.domain.RecordDO;
import com.bootdo.system.service.RecordService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-01 10:55:13
 */
 
@Controller
@RequestMapping("/system/record")
public class RecordController {
	@Autowired
	private RecordService recordService;
	
	@GetMapping()
	@RequiresPermissions("system:record:record")
	String Record(){
	    return "system/record/record";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:record:record")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<RecordDO> recordList = recordService.list(query);
		int total = recordService.count(query);
		PageUtils pageUtils = new PageUtils(recordList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:record:add")
	String add(){
	    return "system/record/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:record:edit")
	String edit(@PathVariable("id") Long id,Model model){
		RecordDO record = recordService.get(id);
		model.addAttribute("record", record);
	    return "system/record/edit";
	}

	@GetMapping("/detail/{id}")
	//@RequiresPermissions("system:record:edit")
	String detail(@PathVariable("id") Long id,Model model){
		model.addAttribute("id", id);
		return "system/record/detail";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	//@RequiresPermissions("system:record:add")
	public R save(@RequestBody RecordDO record){
		if(recordService.save(record)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:record:edit")
	public R update( RecordDO record){
		recordService.update(record);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:record:remove")
	public R remove( Long id){
		if(recordService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:record:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		recordService.batchRemove(ids);
		return R.ok();
	}
	
}
