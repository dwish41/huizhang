package com.bootdo.system.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.bootdo.common.annotation.Log;
import com.bootdo.common.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.system.domain.CompanyDO;
import com.bootdo.system.service.CompanyService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 *
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2022-03-07 14:21:29
 */

@Controller
@RequestMapping("/system/company")
public class CompanyController extends BaseController {
	@Autowired
	private CompanyService companyService;

	@GetMapping()
	@RequiresPermissions("system:company:company")
	String Company(){
	    return "system/company/company";
	}

	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:company:company")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<CompanyDO> companyList = companyService.list(query);
		int total = companyService.count(query);
		PageUtils pageUtils = new PageUtils(companyList, total);
		return pageUtils;
	}

	@GetMapping("/add")
	String add(Model model){
		model.addAttribute("attachId", UUID.randomUUID());
	    return "system/company/add";
	}

	@GetMapping("/edit/{id}")
	String edit(@PathVariable("id") Long id,Model model){
		CompanyDO company = companyService.get(id);
		model.addAttribute("attachId", UUID.randomUUID());
		model.addAttribute("company", company);
	    return "system/company/edit";
	}

	@GetMapping("/detail/{id}")
	String detail(@PathVariable("id") Long id,Model model){
		CompanyDO company = companyService.get(id);
		model.addAttribute("company", company);
		return "system/company/detail";
	}

	/**
	 * 保存
	 */
	@ResponseBody
	@Log("新增企业")
	@PostMapping("/save")
	public R save( CompanyDO company){
		company.setCreateBy(getUserId().toString());
		company.setCreateDate(new Date());
		company.setDelFlag("0");//有效
		if(companyService.save(company)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@Log("修改企业")
	@RequestMapping("/update")
	public R update( CompanyDO company){
		company.setUpdateBy(getUserId().toString());
		company.setUpdateDate(new Date());
		companyService.update(company);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@Log("删除企业")
	@ResponseBody
	public R remove( Long id){
		if(companyService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@Log("删除企业")
	@ResponseBody
	public R remove(@RequestParam("ids[]") Long[] ids){
		companyService.batchRemove(ids);
		return R.ok();
	}

}
