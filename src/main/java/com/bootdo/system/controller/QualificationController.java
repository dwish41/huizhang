package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.QualificationDO;
import com.bootdo.system.domain.SceneryArticleDO;
import com.bootdo.system.service.QualificationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/system/qualification")
@Controller
public class QualificationController extends BaseController {

    @Autowired
    private QualificationService qualificationService;

    private String prefix="system/qualification"  ;


    @RequiresPermissions("system:qualification:list")
    @GetMapping("")
    String goList(Model model) {
        return prefix + "/list";
    }


    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("system:qualification:list")
    public PageUtils list(@RequestParam Map<String, Object> params){
        //查询列表数据
        Query query = new Query(params);
        List<QualificationDO> qualificationDOList = qualificationService.list(query);
        int total = qualificationService.count(query);
        PageUtils pageUtils = new PageUtils(qualificationDOList, total);
        return pageUtils;
    }


    /**
     * 查看
     */
    @GetMapping("/detail")
    public String detail(Long id, Model model){
        QualificationDO qualificationDO = qualificationService.get(id);
        model.addAttribute("qualificationDO", qualificationDO);
        return prefix + "/detail";
    }


    @GetMapping("/add")
    String add(Model model){
        return prefix + "/add";
    }


    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(QualificationDO qualificationDO){
        if(qualificationService.save(qualificationDO)>0){
            return R.ok();
        }
        return R.error();
    }


    /**
     * 删除
     */
    @PostMapping( "/remove")
    @ResponseBody
    public R remove( Long id){
        if(qualificationService.remove(id)>0){
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping( "/batchRemove")
    @ResponseBody
    public R remove(@RequestParam("ids[]") Long[] ids){
        qualificationService.batchRemove(ids);
        return R.ok();
    }


    @GetMapping("/edit")
    String edit(Model model,Long id){
        QualificationDO qualificationDO = qualificationService.get(id);
        model.addAttribute("qualificationDO", qualificationDO);
        return prefix + "/edit";
    }


    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    public R update(QualificationDO qualificationDO){
        qualificationService.update(qualificationDO);
        return R.ok();
    }


    @ResponseBody
    @PostMapping("/qualificationDOList")
    public R qualificationDOList(){
        List<QualificationDO> qualificationDOList = qualificationService.list(null);
        return R.ok().put("qualificationDOList",qualificationDOList);
    }

}
