package com.bootdo.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.system.domain.RecordDetailDO;
import com.bootdo.system.service.RecordDetailService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-01 10:55:13
 */
 
@Controller
@RequestMapping("/system/recordDetail")
public class RecordDetailController {
	@Autowired
	private RecordDetailService recordDetailService;
	
	@GetMapping()
	@RequiresPermissions("system:recordDetail:recordDetail")
	String RecordDetail(){
	    return "system/recordDetail/recordDetail";
	}
	
	@ResponseBody
	@GetMapping("/list")
	//@RequiresPermissions("system:recordDetail:recordDetail")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<RecordDetailDO> recordDetailList = recordDetailService.list(query);
		int total = recordDetailService.count(query);
		PageUtils pageUtils = new PageUtils(recordDetailList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:recordDetail:add")
	String add(){
	    return "system/recordDetail/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:recordDetail:edit")
	String edit(@PathVariable("id") Long id,Model model){
		RecordDetailDO recordDetail = recordDetailService.get(id);
		model.addAttribute("recordDetail", recordDetail);
	    return "system/recordDetail/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:recordDetail:add")
	public R save( RecordDetailDO recordDetail){
		if(recordDetailService.save(recordDetail)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:recordDetail:edit")
	public R update( RecordDetailDO recordDetail){
		recordDetailService.update(recordDetail);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:recordDetail:remove")
	public R remove( Long id){
		if(recordDetailService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:recordDetail:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		recordDetailService.batchRemove(ids);
		return R.ok();
	}
	
}
