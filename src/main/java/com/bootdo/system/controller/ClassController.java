package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.ClassDO;
import com.bootdo.system.service.ClassService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-16 09:24:39
 */
 
@Controller
@RequestMapping("/system/class")
public class ClassController extends BaseController {
	@Autowired
	private ClassService classService;
	
	@GetMapping()
	@RequiresPermissions("system:class:class")
	String Class(){
	    return "system/class/class";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:class:class")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<ClassDO> classList = classService.list(query);
		int total = classService.count(query);
		PageUtils pageUtils = new PageUtils(classList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:class:add")
	String add(){
	    return "system/class/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:class:edit")
	String edit(@PathVariable("id") Long id,Model model){
		ClassDO classDO = classService.get(id);
		model.addAttribute("classDO", classDO);
	    return "system/class/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:class:add")
	public R save( ClassDO classDO){
		classDO.setCreateBy(getUserId().toString());
		if(classService.save(classDO)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:class:edit")
	public R update( ClassDO classDO){
		classService.update(classDO);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:class:remove")
	public R remove( Long id){
		if(classService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping("/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:class:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		classService.batchRemove(ids);
		return R.ok();
	}
	
}
