package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.system.domain.SceneryOrderDO;
import com.bootdo.system.service.SceneryOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@RequestMapping("/scenery/order")
@Controller
public class SceneryOrderController extends BaseController {

    private String prefix="system/order";

    @Autowired
    private SceneryOrderService sceneryOrderService;


    @RequiresPermissions("scenery:order:surroundingList")
    @GetMapping("/surroundingList")
    String goSurroundingList(Model model) {
        return prefix + "/surroundingList";
    }


    @ResponseBody
    @GetMapping("/listSurrounding")
    @RequiresPermissions("scenery:order:surroundingList")
    public PageUtils listSurrounding(@RequestParam Map<String, Object> params){
        params.put("orderType",1);
        //查询列表数据
        Query query = new Query(params);
        List<SceneryOrderDO> orderList = sceneryOrderService.list(query);
        int total = sceneryOrderService.count(query);
        PageUtils pageUtils = new PageUtils(orderList, total);
        return pageUtils;
    }



    @RequiresPermissions("scenery:order:playList")
    @GetMapping("/playList")
    String goPlayList(Model model) {
        return prefix + "/playList";
    }


    @ResponseBody
    @GetMapping("/listPlay")
    @RequiresPermissions("scenery:order:playList")
    public PageUtils listPlay(@RequestParam Map<String, Object> params){
        params.put("orderType",2);
        //查询列表数据
        Query query = new Query(params);
        List<SceneryOrderDO> orderList = sceneryOrderService.list(query);
        int total = sceneryOrderService.count(query);
        PageUtils pageUtils = new PageUtils(orderList, total);
        return pageUtils;
    }

}
