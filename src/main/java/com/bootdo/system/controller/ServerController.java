package com.bootdo.system.controller;


import com.bootdo.common.utils.R;
import com.bootdo.common.utils.ServerUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/server/")
public class ServerController {

    @PostMapping(value = "getData")
    @ResponseBody
    public R server() throws Exception {
        ServerUtils server = new ServerUtils();
        server.copyTo();
        return R.ok(server);
    }

}
