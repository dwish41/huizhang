package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.LinkDO;
import com.bootdo.system.service.LinkService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-16 10:15:54
 */
 
@Controller
@RequestMapping("/system/link")
public class LinkController extends BaseController {
	@Autowired
	private LinkService linkService;
	
	@GetMapping()
	@RequiresPermissions("system:link:link")
	String Link(){
	    return "system/link/link";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:link:link")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<LinkDO> linkList = linkService.list(query);
		int total = linkService.count(query);
		PageUtils pageUtils = new PageUtils(linkList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:link:add")
	String add(Model model){
		model.addAttribute("attachId", UUID.randomUUID());
	    return "system/link/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:link:edit")
	String edit(@PathVariable("id") Long id,Model model){
		LinkDO link = linkService.get(id);
		model.addAttribute("attachId", UUID.randomUUID());
		model.addAttribute("link", link);
	    return "system/link/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:link:add")
	public R save( LinkDO link){
		link.setCreateBy(getUserId().toString());
		if(linkService.save(link)>0){
			return R.ok();
		}
		return R.error();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:link:edit")
	public R update( LinkDO link){
		linkService.update(link);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:link:remove")
	public R remove( Long id){
		if(linkService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:link:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		linkService.batchRemove(ids);
		return R.ok();
	}



	@ResponseBody
	@PostMapping("/linkList")
	public R linkList(){
		List<LinkDO> linkList = linkService.list(null);
		return R.ok().put("linkList",linkList);
	}
	
}
