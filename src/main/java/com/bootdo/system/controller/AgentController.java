package com.bootdo.system.controller;

import com.bootdo.common.annotation.Log;
import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.service.DictService;
import com.bootdo.common.utils.ExcelUtil;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.RoleDO;
import com.bootdo.system.domain.UserDO;
import com.bootdo.system.service.RoleService;
import com.bootdo.system.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RequestMapping("/sys/agent")
@Controller
public class AgentController extends BaseController {
	private String prefix="system/agent"  ;
	@Autowired
	UserService userService;
	@Autowired
	RoleService roleService;
	@Autowired
	DictService dictService;
	@RequiresPermissions("sys:agent:agent")
	@GetMapping("")
	String user(Model model) {
		return prefix + "/agent";
	}

	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		Query query = new Query(params);
		List<UserDO> sysUserList = userService.list(query);
		int total = userService.count(query);
		PageUtils pageUtil = new PageUtils(sysUserList, total);
		return pageUtil;
	}

	/*@RequiresPermissions("sys:user:add")*/
	@Log("添加代理")
	@GetMapping("/add")
	String add(Model model) {
		List<RoleDO> roles = roleService.list();
		model.addAttribute("roles", roles);
		return prefix + "/add";
	}

	/*@RequiresPermissions("sys:user:edit")*/
	@Log("编辑代理")
	@GetMapping("/edit/{id}")
	String edit(Model model, @PathVariable("id") Long id) {
		UserDO userDO = userService.get(id);
		model.addAttribute("user", userDO);
		List<RoleDO> roles = roleService.list(id);
		model.addAttribute("roles", roles);
		return prefix+"/edit";
	}

	@Log("查看代理")
	@GetMapping("/detail/{id}")
	String detail(Model model, @PathVariable("id") Long id) {
		UserDO userDO = userService.get(id);
		model.addAttribute("user", userDO);
		List<RoleDO> roles = roleService.list(id);
		model.addAttribute("roles", roles);
		return prefix+"/detail";
	}

	/*@RequiresPermissions("sys:user:add")*/
	@Log("保存代理")
	@PostMapping("/save")
	@ResponseBody
	R save(UserDO user) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		//代理角色
		user.setRoleId(Constant.AGENT_ROLE);
		//创建人
		user.setUserIdCreate(getUserId());
		if (userService.save(user) > 0) {
			return R.ok();
		}
		return R.error();
	}

	/*@RequiresPermissions("sys:user:edit")*/
	@Log("更新代理")
	@PostMapping("/update")
	@ResponseBody
	R update(UserDO user) {
		//代理角色
		user.setRoleId(Constant.AGENT_ROLE);
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (userService.update(user) > 0) {
			return R.ok();
		}
		return R.error();
	}


	/*@RequiresPermissions("sys:user:remove")*/
	@Log("删除代理")
	@PostMapping("/remove")
	@ResponseBody
	R remove(Long id) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (userService.remove(id) > 0) {
			return R.ok();
		}
		return R.error();
	}

	/*@RequiresPermissions("sys:user:batchRemove")*/
	@Log("批量删除代理")
	@PostMapping("/batchRemove")
	@ResponseBody
	R batchRemove(@RequestParam("ids[]") Long[] userIds) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		int r = userService.batchremove(userIds);
		if (r > 0) {
			return R.ok();
		}
		return R.error();
	}

	@Log("导入代理数据")
	@PostMapping("/importUser")
	public R importData(MultipartFile file, boolean updateSupport) throws Exception {
		ExcelUtil<UserDO> util = new ExcelUtil<UserDO>(UserDO.class);
		List<UserDO> userList = util.importExcel(file.getInputStream());
		R r = userService.importUser(userList, updateSupport, getUser());
		return r;
	}

	/***
	 * 获取代理列表数据
	 * @param userDO
	 * @return
	 */
	@GetMapping("/loadAgentList")
	@ResponseBody
	public List<UserDO> loadAgentList(UserDO userDO) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("roleId",userDO.getRoleId());
		map.put("name", userDO.getName());
		// 查询列表数据
		List<UserDO> sysUserList = userService.list(map);
		//删除是自己的代理
        Iterator<UserDO> iterator = sysUserList.iterator();
        while (iterator.hasNext()){
            UserDO next = iterator.next();
            if (next.getUserId() == userDO.getUserId()){
                iterator.remove();
            }
        }
        return sysUserList;
	}
}
