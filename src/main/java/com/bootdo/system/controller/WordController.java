package com.bootdo.system.controller;

import java.util.List;
import java.util.Map;

import com.bootdo.common.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.bouncycastle.math.raw.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.system.domain.WordDO;
import com.bootdo.system.service.WordService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 *
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2022-03-03 10:07:22
 */

@Controller
@RequestMapping("/system/word")
public class WordController extends BaseController {
	@Autowired
	private WordService wordService;

	@GetMapping()
	@RequiresPermissions("system:word:word")
	String Word(){
	    return "system/word/word";
	}

	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:word:word")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<WordDO> wordList = wordService.list(query);
		int total = wordService.count(query);
		PageUtils pageUtils = new PageUtils(wordList, total);
		return pageUtils;
	}

	@GetMapping("/add")
	/*@RequiresPermissions("system:word:add")*/
	String add(Model model){
		model.addAttribute("userDo",getUser());
		return "system/word/add";
	}

	@GetMapping("/edit/{id}")
	/*@RequiresPermissions("system:word:edit")*/
	String edit(@PathVariable("id") Long id,Model model){
		WordDO word = wordService.get(id);
		model.addAttribute("word", word);
		model.addAttribute("userDo",getUser());
	    return "system/word/edit";
	}

	@GetMapping("/detail/{id}")
		/*@RequiresPermissions("system:word:edit")*/
	String detail(@PathVariable("id") Long id,Model model){
		WordDO word = wordService.get(id);
		model.addAttribute("word", word);
		return "system/word/detail";
	}

	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	/*@RequiresPermissions("system:word:add")*/
	public R save( WordDO word){
		if(wordService.save(word)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	/*@RequiresPermissions("system:word:edit")*/
	public R update( WordDO word){
		wordService.update(word);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	/*@RequiresPermissions("system:word:remove")*/
	public R remove( Long id){
		if(wordService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	/*@RequiresPermissions("system:word:batchRemove")*/
	public R remove(@RequestParam("ids[]") Long[] ids){
		wordService.batchRemove(ids);
		return R.ok();
	}

}
