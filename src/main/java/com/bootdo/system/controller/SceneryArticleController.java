package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.ArticleDO;
import com.bootdo.system.domain.SceneryArticleDO;
import com.bootdo.system.service.SceneryArticleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RequestMapping("/scenery/article")
@Controller
public class SceneryArticleController extends BaseController {

    private String prefix="system/article"  ;

    @Autowired
    private SceneryArticleService sceneryArticleService;

    @RequiresPermissions("scenery:article")
    @GetMapping("/article/{articleType}")
    String goNewsList(@PathVariable("articleType") String articleType,Model model) {
        model.addAttribute("articleType", articleType);
        return prefix + "/newsList";
    }


    @ResponseBody
    @GetMapping("/{type}")
    @RequiresPermissions("scenery:article")
    public PageUtils listNews(@PathVariable("type") String type,@RequestParam Map<String, Object> params){
        params.put("articleType",type);
        //查询列表数据
        Query query = new Query(params);
        List<SceneryArticleDO> articleList = sceneryArticleService.list(query);
        int total = sceneryArticleService.count(query);
        PageUtils pageUtils = new PageUtils(articleList, total);
        return pageUtils;
    }


    /**
     * 查看
     */
    @GetMapping("/detail")
    public String detail(Long id, Model model,String articleType){
        SceneryArticleDO sceneryArticleDO = sceneryArticleService.get(id,articleType);
        model.addAttribute("sceneryArticleDO", sceneryArticleDO);
        return prefix + "/newsList_detail";
    }


    @GetMapping("/add")
    String add(Model model,String type){
        model.addAttribute("attachId", UUID.randomUUID());
        model.addAttribute("articleType", type);
        return prefix + "/newsList_add";
    }


    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(SceneryArticleDO sceneryArticleDO){
        if(sceneryArticleService.save(sceneryArticleDO)>0){
            return R.ok();
        }
        return R.error();
    }


    /**
     * 删除
     */
    @PostMapping( "/remove")
    @ResponseBody
    public R remove( Long id){
        if(sceneryArticleService.remove(id)>0){
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping( "/batchRemove")
    @ResponseBody
    public R remove(@RequestParam("ids[]") Long[] ids){
        sceneryArticleService.batchRemove(ids);
        return R.ok();
    }


    @GetMapping("/edit")
    String edit(Long id,Model model,String articleType){
        SceneryArticleDO sceneryArticleDO = sceneryArticleService.get(id,articleType);
        model.addAttribute("attachId", UUID.randomUUID());
        model.addAttribute("sceneryArticleDO", sceneryArticleDO);
        return prefix + "/newsList_edit";
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    public R update(SceneryArticleDO sceneryArticleDO){
        sceneryArticleService.update(sceneryArticleDO);
        return R.ok();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/updateFor")
    public R updateFor(SceneryArticleDO sceneryArticleDO){
        SceneryArticleDO sceneryArticleDO1 = sceneryArticleService.get(null,sceneryArticleDO.getArticleType());
        if(sceneryArticleDO1 == null) {
            sceneryArticleService.save(sceneryArticleDO);
        }else {
            sceneryArticleService.update(sceneryArticleDO);
        }
        return R.ok();
    }

    @RequiresPermissions("scenery:article:gywm")
    @GetMapping("/gywm/{articleType}")
    String goGywmList(@PathVariable("articleType") String articleType,Model model) {
        SceneryArticleDO sceneryArticleDO = sceneryArticleService.get(null,articleType);
        model.addAttribute("articleType", articleType);
        if(null == sceneryArticleDO) {
            model.addAttribute("sceneryArticleDO", new SceneryArticleDO());
        }else {
            model.addAttribute("sceneryArticleDO", sceneryArticleDO);
        }
        return prefix + "/gywmList";
    }


}
