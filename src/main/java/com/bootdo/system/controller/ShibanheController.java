package com.bootdo.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Controller
public class ShibanheController {

    private String prefix="shibanhe"  ;

    /*@GetMapping("/")
    String goIndex(Model model) {
        return prefix + "/index";
    }*/


    @GetMapping("/dynamic")
    String goDynamic(Model model) {
        return prefix + "/dynamic";
    }


    @GetMapping("/impression")
    String goImpression(Model model) {
        return prefix + "/impression";
    }


    @GetMapping("/online")
    String goOnline(Model model) {
        return prefix + "/online";
    }


    @GetMapping("/play")
    String goPlay(Model model,String index) {
        model.addAttribute("index", index);
        return prefix + "/play";
    }


    @GetMapping("/service")
    String goService(Model model) {
        return prefix + "/service";
    }


    @GetMapping("/walking")
    String goWalking(Model model) {
        return prefix + "/walking";
    }


    @GetMapping("/strategy")
    String goStrategy(Model model) {
        return prefix + "/strategy";
    }

    @GetMapping("/surrounding")
    String goSurrounding(Model model) {
        return prefix + "/surrounding";
    }

    @GetMapping("/detail")
    String goDetail(Model model,Long id,String articleType) {
        model.addAttribute("id", id);
        model.addAttribute("articleType", articleType);
        return prefix + "/detail";
    }

    @GetMapping("/more")
    String goDetail(Model model,String articleType) {
        model.addAttribute("articleType", articleType);
        return prefix + "/more";
    }

    @GetMapping("/search")
    String goSearch(Model model, HttpServletRequest request) throws UnsupportedEncodingException {
        String searchVal= request.getParameter("searchVal");
        model.addAttribute("searchVal", searchVal);
        return prefix + "/search";
    }

    @GetMapping("/shopping")
    String goShopping(Model model, HttpServletRequest request) throws UnsupportedEncodingException {
        return prefix + "/shopping";
    }

    @GetMapping("/detailPrice")
    String goDetailPrice(Model model, Long id) throws UnsupportedEncodingException {
        model.addAttribute("id", id);
        return prefix + "/detailPrice";
    }

    @GetMapping("/shoppingDetail")
    String goShoppingDetail(Model model, Long id) throws UnsupportedEncodingException {
        model.addAttribute("id", id);
        return prefix + "/shopping_detail";
    }

    @GetMapping("/qualificationMore")
    String goQualificationMore(Model model) {
        return prefix + "/qualificationMore";
    }
}
