package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.MessageDO;
import com.bootdo.system.service.MessageService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/system/message")
@Controller
public class MessageController extends BaseController {

    @Autowired
    private MessageService messageService;


    @GetMapping()
    @RequiresPermissions("system:message:list")
    String message() {
        return "system/message/list";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("system:message:list")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        params.put("type","1");
        // 查询列表数据
        Query query = new Query(params);
        List<MessageDO> messageDOList = messageService.list(query);
        int total = messageService.count(query);
        PageUtils pageUtils = new PageUtils(messageDOList, total);
        return pageUtils;
    }


    @GetMapping("/complaintsList")
    @RequiresPermissions("system:message:complaintsList")
    String complaintsList() {
        return "system/message/complaintsList";
    }

    @ResponseBody
    @GetMapping("/listComplaints")
    @RequiresPermissions("system:message:complaintsList")
    public PageUtils listComplaints(@RequestParam Map<String, Object> params) {
        params.put("type","2");
        // 查询列表数据
        Query query = new Query(params);
        List<MessageDO> messageDOList = messageService.list(query);
        int total = messageService.count(query);
        PageUtils pageUtils = new PageUtils(messageDOList, total);
        return pageUtils;
    }


    @GetMapping("/investigationList")
    @RequiresPermissions("system:message:investigationList")
    String investigationList() {
        return "system/message/investigationList";
    }

    @ResponseBody
    @GetMapping("/listInvestigation")
    @RequiresPermissions("system:message:investigationList")
    public PageUtils listInvestigation(@RequestParam Map<String, Object> params) {
        params.put("type","3");
        // 查询列表数据
        Query query = new Query(params);
        List<MessageDO> messageDOList = messageService.list(query);
        int total = messageService.count(query);
        PageUtils pageUtils = new PageUtils(messageDOList, total);
        return pageUtils;
    }


    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(MessageDO messageDO){
        if(messageService.save(messageDO)>0){
            return R.ok();
        }
        return R.error();
    }


}
