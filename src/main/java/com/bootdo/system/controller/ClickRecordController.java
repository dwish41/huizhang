package com.bootdo.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.system.domain.ClickRecordDO;
import com.bootdo.system.service.ClickRecordService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-03 11:00:50
 */
 
@Controller
@RequestMapping("/system/clickRecord")
public class ClickRecordController {
	@Autowired
	private ClickRecordService clickRecordService;
	
	@GetMapping()
	@RequiresPermissions("system:clickRecord:clickRecord")
	String ClickRecord(){
	    return "system/clickRecord/clickRecord";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:clickRecord:clickRecord")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<ClickRecordDO> clickRecordList = clickRecordService.list(query);
		int total = clickRecordService.count(query);
		PageUtils pageUtils = new PageUtils(clickRecordList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:clickRecord:add")
	String add(){
	    return "system/clickRecord/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:clickRecord:edit")
	String edit(@PathVariable("id") Long id,Model model){
		ClickRecordDO clickRecord = clickRecordService.get(id);
		model.addAttribute("clickRecord", clickRecord);
	    return "system/clickRecord/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	//@RequiresPermissions("system:clickRecord:add")
	public R save( ClickRecordDO clickRecord){
		if(clickRecordService.save(clickRecord)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:clickRecord:edit")
	public R update( ClickRecordDO clickRecord){
		clickRecordService.update(clickRecord);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:clickRecord:remove")
	public R remove( Long id){
		if(clickRecordService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:clickRecord:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		clickRecordService.batchRemove(ids);
		return R.ok();
	}
	
}
