package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.SetDO;
import com.bootdo.system.service.SetService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.UUID;

@RequestMapping("/sys/sysSet")
@Controller
public class SetController extends BaseController {

    private String prefix="system/set"  ;

    @Autowired
    private SetService setService;

    @RequiresPermissions("sys:sysSet:sysSet")
    @GetMapping("/sysSet")
    String user(Model model) {
        return prefix + "/sysSet";
    }


    @ResponseBody
    @PostMapping("/list")
    public R list() {
        List<SetDO> setDOList = setService.list(null);
        return R.ok().put("setDOList",setDOList);
    }


    @ResponseBody
    @PostMapping("/updateCompany")
    public R updateCompany(String companyName,String copyright,String recordNo,String technicalSupport) {
        int result= setService.updateCompany(companyName,copyright,recordNo,technicalSupport);
        return R.ok();
    }

    @ResponseBody
    @PostMapping("/updatePhone")
    public R updatePhone(String consultPhone,String complainPhone,String faxPhone) {
        int result= setService.updatePhone(consultPhone,complainPhone,faxPhone);
        return R.ok();
    }


    @ResponseBody
    @PostMapping("/updateWeChatNum")
    public R updateWeChatNum(String weChatNum) {
        int result= setService.updateWeChatNum(weChatNum);
        return R.ok();
    }


    // 1：底部公司  2:咨询电话 3：微信号 4:图片
    @ResponseBody
    @GetMapping("/getSet")
    public R getSet(String type) {
        List<SetDO> setDOList = setService.list(type);
        return R.ok().put("setDOList",setDOList);
    }

}
