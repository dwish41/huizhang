package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.OrderDO;
import com.bootdo.system.service.OrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RequestMapping("/system/order")
@Controller
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;


    @GetMapping()
    @RequiresPermissions("system:order:list")
    String order() {
        return "system/order/list";
    }


    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("system:order:list")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        // 查询列表数据
        Query query = new Query(params);
        List<OrderDO> orderDOList = orderService.list(query);
        int total = orderService.count(query);
        PageUtils pageUtils = new PageUtils(orderDOList, total);
        return pageUtils;
    }


    @ResponseBody
    @PostMapping("/getClickNum")
    public R getClickNum(String ticketTime){
        Integer clickNum= orderService.getNum(ticketTime);
        return R.ok().put("clickNum",clickNum);
    }


    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(OrderDO orderDO){
        if(orderService.save(orderDO)>0){
            return R.ok();
        }
        return R.error();
    }



}
