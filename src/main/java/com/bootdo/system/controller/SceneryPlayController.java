package com.bootdo.system.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.SceneryArticleDO;
import com.bootdo.system.domain.SceneryPlayDO;
import com.bootdo.system.service.SceneryPlayService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RequestMapping("/scenery/play")
@Controller
public class SceneryPlayController extends BaseController {

    private String prefix="system/play";

    @Autowired
    private SceneryPlayService sceneryPlayService;


    @RequiresPermissions("scenery:play:list")
    @GetMapping("")
    String goNewsList(Model model) {
        return prefix + "/list";
    }


    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("scenery:play:list")
    public PageUtils list(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
        List<SceneryPlayDO> articleList = sceneryPlayService.list(query);
        int total = sceneryPlayService.count(query);
        PageUtils pageUtils = new PageUtils(articleList, total);
        return pageUtils;
    }


    @GetMapping("/edit")
    String edit(Long id,Model model){
        SceneryPlayDO sceneryPlayDO = sceneryPlayService.get(id);
        model.addAttribute("attachId", UUID.randomUUID());
        model.addAttribute("sceneryPlayDO", sceneryPlayDO);
        return prefix + "/edit";
    }

    @ResponseBody
    @RequestMapping("/update")
    public R update(SceneryPlayDO sceneryPlayDO){
        sceneryPlayService.update(sceneryPlayDO);
        return R.ok();
    }

    @ResponseBody
    @RequestMapping("/getPlayPicture")
    public R getPlayPicture(){
        List<SceneryPlayDO> sceneryPlayList = sceneryPlayService.list(null);
        return R.ok().put("sceneryPlayList",sceneryPlayList);
    }
}
