package com.bootdo.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.bootdo.common.controller.BaseController;
import com.bootdo.system.domain.ClassDO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.system.domain.ArticleDO;
import com.bootdo.system.service.ArticleService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-02 13:46:07
 */
 
@Controller
@RequestMapping("/system/article")
public class ArticleController extends BaseController {
	@Autowired
	private ArticleService articleService;
	
	@GetMapping()
	@RequiresPermissions("system:article:article")
	String Article(){
	    return "system/article/article";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:article:article")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<ArticleDO> articleList = articleService.list(query);
		int total = articleService.count(query);
		PageUtils pageUtils = new PageUtils(articleList, total);
		return pageUtils;
	}

	@ResponseBody
	@GetMapping("/articleList")
	public List<ArticleDO> articleList(ArticleDO articleDO){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("classId",articleDO.getClassId());
		List<ArticleDO> articleList = articleService.articleList(map);
		return articleList;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:article:add")
	String add(Model model){
		model.addAttribute("attachId", UUID.randomUUID());
	    return "system/article/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:article:edit")
	String edit(@PathVariable("id") Long id,Model model){
		ArticleDO article = articleService.get(id);
		model.addAttribute("attachId", UUID.randomUUID());
		model.addAttribute("article", article);
	    return "system/article/edit";
	}

	/**
	 * 查看
	 */
	@GetMapping("/detail/{id}")
	//@RequiresPermissions("system:article:detail")
	public String detail(@PathVariable("id") Long id,Model model){
		ArticleDO article = articleService.get(id);
		model.addAttribute("article", article);
		return "system/article/detail";
	}

	/**
	 * 获取文章详情
	 */
	@GetMapping("/getDetail")
	@ResponseBody
	//@RequiresPermissions("system:article:detail")
	public ArticleDO getDetail(ArticleDO articleDO){
		articleDO = articleService.get(articleDO.getId());
		return articleDO;
	}

	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:article:add")
	public R save( ArticleDO article){
		article.setCreateBy(getUserId().toString());
		article.setUpdateBy(getUserId().toString());
		if(articleService.save(article)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:article:edit")
	public R update( ArticleDO article){
		article.setUpdateBy(getUserId().toString());
		articleService.update(article);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:article:remove")
	public R remove( Long id){
		if(articleService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:article:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		articleService.batchRemove(ids);
		return R.ok();
	}

	/**
	 * 根据大类获取小类
	 * @param bigClass
	 * @return
	 */
	@PostMapping( "/getSmallClass")
	@ResponseBody
	public R getSmallClass( String bigClass) {
		List<ClassDO> list = articleService.getSmallClass(bigClass);
		return R.ok(list);
	}
}
