package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2021-02-19 09:40:02
 */
public class EnergyLogDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//用户id
	private Long userId;
	//积分能量值类型
	private String energyType;
	//积分能量值说明
	private String energyDescription;
	//
	private Date createDate;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：积分能量值类型
	 */
	public void setEnergyType(String energyType) {
		this.energyType = energyType;
	}
	/**
	 * 获取：积分能量值类型
	 */
	public String getEnergyType() {
		return energyType;
	}
	/**
	 * 设置：积分能量值说明
	 */
	public void setEnergyDescription(String energyDescription) {
		this.energyDescription = energyDescription;
	}
	/**
	 * 获取：积分能量值说明
	 */
	public String getEnergyDescription() {
		return energyDescription;
	}
	/**
	 * 设置：
	 */
	public void setcreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：
	 */
	public Date getcreateDate() {
		return createDate;
	}
}
