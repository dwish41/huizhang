package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-01 10:55:13
 */
public class RecordDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//访问者手机号
	private String visitPhone;
	//访问者姓名
	private String visitName;
	//访问次数
	private Long visitCount;
	//访问类型  1.访问登录  2.拨号咨询
	private String visitType;

	private Date lastVisitTime;

	//访问ip
	private String visitIp;
	//访问地址
	private String visitAddress;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：访问者手机号
	 */
	public void setVisitPhone(String visitPhone) {
		this.visitPhone = visitPhone;
	}
	/**
	 * 获取：访问者手机号
	 */
	public String getVisitPhone() {
		return visitPhone;
	}
	/**
	 * 设置：访问者姓名
	 */
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	/**
	 * 获取：访问者姓名
	 */
	public String getVisitName() {
		return visitName;
	}
	/**
	 * 设置：访问次数
	 */
	public void setVisitCount(Long visitCount) {
		this.visitCount = visitCount;
	}
	/**
	 * 获取：访问次数
	 */
	public Long getVisitCount() {
		return visitCount;
	}
	/**
	 * 设置：访问类型  1.访问登录  2.拨号咨询
	 */
	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}
	/**
	 * 获取：访问类型  1.访问登录  2.拨号咨询
	 */
	public String getVisitType() {
		return visitType;
	}

	public Date getLastVisitTime() {
		return lastVisitTime;
	}

	public void setLastVisitTime(Date lastVisitTime) {
		this.lastVisitTime = lastVisitTime;
	}

	public String getVisitIp() {
		return visitIp;
	}

	public void setVisitIp(String visitIp) {
		this.visitIp = visitIp;
	}

	public String getVisitAddress() {
		return visitAddress;
	}

	public void setVisitAddress(String visitAddress) {
		this.visitAddress = visitAddress;
	}
}
