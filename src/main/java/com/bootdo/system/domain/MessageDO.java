package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 留言
 *
 * @author hcf
 * @email 1992lcg@163.com
 * @date 2020-10-27 14:28:36
 */
public class MessageDO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String messageContent;
    private String replyContent;
    private Date messageCreateTime;
    private Date replyCreateTime;
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public Date getMessageCreateTime() {
        return messageCreateTime;
    }

    public void setMessageCreateTime(Date messageCreateTime) {
        this.messageCreateTime = messageCreateTime;
    }

    public Date getReplyCreateTime() {
        return replyCreateTime;
    }

    public void setReplyCreateTime(Date replyCreateTime) {
        this.replyCreateTime = replyCreateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
