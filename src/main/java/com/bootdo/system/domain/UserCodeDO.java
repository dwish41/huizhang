package com.bootdo.system.domain;

import lombok.Data;

import java.util.Date;

/**
 * 用户验证码发送记录DO
 *
 * @author zhouxm
 */
@Data
public class UserCodeDO {
    /**
     * 主键id
     */
    private String id;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 短信验证码
     */
    private String code;
    /**
     * 验证码发送时间
     */
    private Date sendTime;
    /**
     * 验证码是否使用
     */
    private String isUsed;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

}
