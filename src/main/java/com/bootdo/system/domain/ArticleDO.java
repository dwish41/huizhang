package com.bootdo.system.domain;

import com.bootdo.common.domain.FileDO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-02 13:46:07
 */
public class ArticleDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//类别表主键id
	private Long classId;
	//文章标题
	private String title;
	//文章介绍
	private String articleIntroduction;
	//作者介绍
	private String authorIntroduction;
	//文章内容
	private String articleContent;
	//创建人
	private String createBy;
	//创建时间
	private Date createDate;
	//修改人
	private String updateBy;
	//修改时间
	private Date updateDate;
	//是否有效  0.有效  1.无效
	private String delFlag;
	//点击量
	private Long viewCount;
	//点赞量
	private Long goodCount;
	//大类
	private String bigClass;
	//小类
	private String smallClass;
	//文章状态  0.发布  1.草稿
	private String articleStatus;
	//文件路径
	private String attachId;

	private String recordType;

	private List<FileDO> filePathList;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：类别表主键id
	 */
	public void setClassId(Long classId) {
		this.classId = classId;
	}
	/**
	 * 获取：类别表主键id
	 */
	public Long getClassId() {
		return classId;
	}
	/**
	 * 设置：文章标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：文章标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：文章介绍
	 */
	public void setArticleIntroduction(String articleIntroduction) {
		this.articleIntroduction = articleIntroduction;
	}
	/**
	 * 获取：文章介绍
	 */
	public String getArticleIntroduction() {
		return articleIntroduction;
	}
	/**
	 * 设置：作者介绍
	 */
	public void setAuthorIntroduction(String authorIntroduction) {
		this.authorIntroduction = authorIntroduction;
	}
	/**
	 * 获取：作者介绍
	 */
	public String getAuthorIntroduction() {
		return authorIntroduction;
	}
	/**
	 * 设置：文章内容
	 */
	public void setArticleContent(String articleContent) {
		this.articleContent = articleContent;
	}
	/**
	 * 获取：文章内容
	 */
	public String getArticleContent() {
		return articleContent;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：修改人
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：修改人
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	/**
	 * 设置：是否有效  0.有效  1.无效
	 */
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	/**
	 * 获取：是否有效  0.有效  1.无效
	 */
	public String getDelFlag() {
		return delFlag;
	}
	/**
	 * 设置：点击量
	 */
	public void setViewCount(Long viewCount) {
		this.viewCount = viewCount;
	}
	/**
	 * 获取：点击量
	 */
	public Long getViewCount() {
		return viewCount;
	}
	/**
	 * 设置：点赞量
	 */
	public void setGoodCount(Long goodCount) {
		this.goodCount = goodCount;
	}
	/**
	 * 获取：点赞量
	 */
	public Long getGoodCount() {
		return goodCount;
	}
	/**
	 * 设置：大类
	 */
	public void setBigClass(String bigClass) {
		this.bigClass = bigClass;
	}
	/**
	 * 获取：大类
	 */
	public String getBigClass() {
		return bigClass;
	}
	/**
	 * 设置：小类
	 */
	public void setSmallClass(String smallClass) {
		this.smallClass = smallClass;
	}
	/**
	 * 获取：小类
	 */
	public String getSmallClass() {
		return smallClass;
	}
	/**
	 * 设置：文章状态  0.发布  1.草稿
	 */
	public void setArticleStatus(String articleStatus) {
		this.articleStatus = articleStatus;
	}
	/**
	 * 获取：文章状态  0.发布  1.草稿
	 */
	public String getArticleStatus() {
		return articleStatus;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public List<FileDO> getFilePathList() {
		return filePathList;
	}

	public void setFilePathList(List<FileDO> filePathList) {
		this.filePathList = filePathList;
	}
}
