package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 *
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2022-03-07 14:21:29
 */
public class CompanyDO implements Serializable {
	private static final long serialVersionUID = 1L;

	//
	private Long id;
	//企业名称
	private String companyName;
	//法人代表
	private String applyName;
	//法人代理身份证号码
	private String applyCardNumber;
	//法人手机号码
	private String applyMobile;
	//企业代码类型
	private String companyType;
	//企业代码
	private String companyCode;
	//省地区code
	private Long proviceAreaId;
	//市地区code
	private Long cityAreaId;
	//区县地区code
	private Long qxAreaId;
	//详细地址
	private String liveAddress;
	//营业执照图片Id
	private String companyLicensePhoto;
	//企业logo图片id
	private String companyLogoPhoto;
	//企业账号
	private String companyUsername;
	//企业账号密码
	private String companyPassword;
	//
	private Date createDate;
	//
	private String createBy;
	//
	private Date updateDate;
	//
	private String updateBy;
	//0 有效  1无效
	private String delFlag;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：企业名称
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	/**
	 * 获取：企业名称
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * 设置：法人代表
	 */
	public void setApplyName(String applyName) {
		this.applyName = applyName;
	}
	/**
	 * 获取：法人代表
	 */
	public String getApplyName() {
		return applyName;
	}
	/**
	 * 设置：法人代理身份证号码
	 */
	public void setApplyCardNumber(String applyCardNumber) {
		this.applyCardNumber = applyCardNumber;
	}
	/**
	 * 获取：法人代理身份证号码
	 */
	public String getApplyCardNumber() {
		return applyCardNumber;
	}
	/**
	 * 设置：法人手机号码
	 */
	public void setApplyMobile(String applyMobile) {
		this.applyMobile = applyMobile;
	}
	/**
	 * 获取：法人手机号码
	 */
	public String getApplyMobile() {
		return applyMobile;
	}
	/**
	 * 设置：企业代码类型
	 */
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	/**
	 * 获取：企业代码类型
	 */
	public String getCompanyType() {
		return companyType;
	}
	/**
	 * 设置：企业代码
	 */
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	/**
	 * 获取：企业代码
	 */
	public String getCompanyCode() {
		return companyCode;
	}
	/**
	 * 设置：省地区code
	 */
	public void setProviceAreaId(Long proviceAreaId) {
		this.proviceAreaId = proviceAreaId;
	}
	/**
	 * 获取：省地区code
	 */
	public Long getProviceAreaId() {
		return proviceAreaId;
	}
	/**
	 * 设置：市地区code
	 */
	public void setCityAreaId(Long cityAreaId) {
		this.cityAreaId = cityAreaId;
	}
	/**
	 * 获取：市地区code
	 */
	public Long getCityAreaId() {
		return cityAreaId;
	}
	/**
	 * 设置：区县地区code
	 */
	public void setQxAreaId(Long qxAreaId) {
		this.qxAreaId = qxAreaId;
	}
	/**
	 * 获取：区县地区code
	 */
	public Long getQxAreaId() {
		return qxAreaId;
	}
	/**
	 * 设置：详细地址
	 */
	public void setLiveAddress(String liveAddress) {
		this.liveAddress = liveAddress;
	}
	/**
	 * 获取：详细地址
	 */
	public String getLiveAddress() {
		return liveAddress;
	}
	/**
	 * 设置：营业执照图片Id
	 */
	public void setCompanyLicensePhoto(String companyLicensePhoto) {
		this.companyLicensePhoto = companyLicensePhoto;
	}
	/**
	 * 获取：营业执照图片Id
	 */
	public String getCompanyLicensePhoto() {
		return companyLicensePhoto;
	}
	/**
	 * 设置：企业logo图片id
	 */
	public void setCompanyLogoPhoto(String companyLogoPhoto) {
		this.companyLogoPhoto = companyLogoPhoto;
	}
	/**
	 * 获取：企业logo图片id
	 */
	public String getCompanyLogoPhoto() {
		return companyLogoPhoto;
	}
	/**
	 * 设置：企业账号
	 */
	public void setCompanyUsername(String companyUsername) {
		this.companyUsername = companyUsername;
	}
	/**
	 * 获取：企业账号
	 */
	public String getCompanyUsername() {
		return companyUsername;
	}
	/**
	 * 设置：企业账号密码
	 */
	public void setCompanyPassword(String companyPassword) {
		this.companyPassword = companyPassword;
	}
	/**
	 * 获取：企业账号密码
	 */
	public String getCompanyPassword() {
		return companyPassword;
	}
	/**
	 * 设置：
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	/**
	 * 设置：
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：0 有效  1无效
	 */
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	/**
	 * 获取：0 有效  1无效
	 */
	public String getDelFlag() {
		return delFlag;
	}
}
