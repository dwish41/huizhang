package com.bootdo.system.domain;

import java.io.Serializable;


/**
 * 系统设置基本配置
 *
 * @author hcf
 * @email 1992lcg@163.com
 * @date 2020-10-20 14:28:36
 */
public class SetDO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String setCode;
    private String setValue;
    private String szflag;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSetCode() {
        return setCode;
    }

    public void setSetCode(String setCode) {
        this.setCode = setCode;
    }

    public String getSetValue() {
        return setValue;
    }

    public void setSetValue(String setValue) {
        this.setValue = setValue;
    }

    public String getSzflag() {
        return szflag;
    }

    public void setSzflag(String szflag) {
        this.szflag = szflag;
    }
}
