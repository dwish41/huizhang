package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-01 10:55:13
 */
public class RecordDetailDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	private Long recordId;
	//访问者手机号
	private String visitPhone;
	//访问者姓名
	private String visitName;
	//访问时间
	private Date visitTime;
	//访问ip
	private String visitIp;
	//访问地址
	private String visitAddress;
	//经度
	private String visitLongitude;
	//维度
	private String visitLatitude;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：访问者手机号
	 */
	public void setVisitPhone(String visitPhone) {
		this.visitPhone = visitPhone;
	}
	/**
	 * 获取：访问者手机号
	 */
	public String getVisitPhone() {
		return visitPhone;
	}
	/**
	 * 设置：访问者姓名
	 */
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	/**
	 * 获取：访问者姓名
	 */
	public String getVisitName() {
		return visitName;
	}
	/**
	 * 设置：访问时间
	 */
	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}
	/**
	 * 获取：访问时间
	 */
	public Date getVisitTime() {
		return visitTime;
	}
	/**
	 * 设置：访问ip
	 */
	public void setVisitIp(String visitIp) {
		this.visitIp = visitIp;
	}
	/**
	 * 获取：访问ip
	 */
	public String getVisitIp() {
		return visitIp;
	}
	/**
	 * 设置：访问地址
	 */
	public void setVisitAddress(String visitAddress) {
		this.visitAddress = visitAddress;
	}
	/**
	 * 获取：访问地址
	 */
	public String getVisitAddress() {
		return visitAddress;
	}
	/**
	 * 设置：经度
	 */
	public void setVisitLongitude(String visitLongitude) {
		this.visitLongitude = visitLongitude;
	}
	/**
	 * 获取：经度
	 */
	public String getVisitLongitude() {
		return visitLongitude;
	}
	/**
	 * 设置：维度
	 */
	public void setVisitLatitude(String visitLatitude) {
		this.visitLatitude = visitLatitude;
	}
	/**
	 * 获取：维度
	 */
	public String getVisitLatitude() {
		return visitLatitude;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
