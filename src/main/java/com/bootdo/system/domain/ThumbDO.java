package com.bootdo.system.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 点赞
 *
 * @author zhouxm
 */
@Data
public class ThumbDO implements Serializable {
    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 商品id
     */
    private String productId;
    /**
     * 1表示点赞，0表示取消点赞
     */
    private String enable;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtModified;
}
