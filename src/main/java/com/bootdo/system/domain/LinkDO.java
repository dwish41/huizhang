package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-16 10:15:54
 */
public class LinkDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//链接名称
	private String linkName;
	//链接地址
	private String linkUrl;
	//文件地址id
	private String attachId;
	//创建时间
	private Date createDate;
	//创建人
	private String createBy;
	//0.有效  1.无效
	private String delFlag;
	//链接内容
	private String linkContent;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：链接名称
	 */
	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}
	/**
	 * 获取：链接名称
	 */
	public String getLinkName() {
		return linkName;
	}
	/**
	 * 设置：链接地址
	 */
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
	/**
	 * 获取：链接地址
	 */
	public String getLinkUrl() {
		return linkUrl;
	}
	/**
	 * 设置：文件地址id
	 */
	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
	/**
	 * 获取：文件地址id
	 */
	public String getAttachId() {
		return attachId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：0.有效  1.无效
	 */
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	/**
	 * 获取：0.有效  1.无效
	 */
	public String getDelFlag() {
		return delFlag;
	}
	/**
	 * 设置：链接内容
	 */
	public void setLinkContent(String linkContent) {
		this.linkContent = linkContent;
	}
	/**
	 * 获取：链接内容
	 */
	public String getLinkContent() {
		return linkContent;
	}
}
