package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2021-02-19 09:51:10
 */
public class EnergyCouponDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//用户id
	private Long userId;
	//优惠券类型
	private String couponType;
	//优惠券说明
	private String couponDescrption;
	//
	private Date createDate;
	//是否兑换  1.已兑换 2.未兑换
	private String exchangeType;
	//是否使用  1.已使用 2.未使用
	private String usedType;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：优惠券类型
	 */
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	/**
	 * 获取：优惠券类型
	 */
	public String getCouponType() {
		return couponType;
	}
	/**
	 * 设置：优惠券说明
	 */
	public void setCouponDescrption(String couponDescrption) {
		this.couponDescrption = couponDescrption;
	}
	/**
	 * 获取：优惠券说明
	 */
	public String getCouponDescrption() {
		return couponDescrption;
	}
	/**
	 * 设置：
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：是否兑换  1.已兑换 2.未兑换
	 */
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	/**
	 * 获取：是否兑换  1.已兑换 2.未兑换
	 */
	public String getExchangeType() {
		return exchangeType;
	}
	/**
	 * 设置：是否使用  1.已使用 2.未使用
	 */
	public void setUsedType(String usedType) {
		this.usedType = usedType;
	}
	/**
	 * 获取：是否使用  1.已使用 2.未使用
	 */
	public String getUsedType() {
		return usedType;
	}
}
