package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-03 11:00:50
 */
public class ClickRecordDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//手机号
	private String clickPhone;
	//姓名
	private String clickName;
	//创建时间
	private Date createTime;
	//访问ip
	private String clickIp;
	//访问地址
	private String clickAddress;
	//经度
	private String clickLongitude;
	//维度
	private String clickLatitude;
	//文章主表id
	private Long articleId;
	//是否有效 0.有效  1.无效
	private String delFlag;
	//记录状态  1.点击  2.点赞
	private String recordType;
	//状态  1.未点赞  2.已点赞
	private String recordStatus;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：手机号
	 */
	public void setClickPhone(String clickPhone) {
		this.clickPhone = clickPhone;
	}
	/**
	 * 获取：手机号
	 */
	public String getClickPhone() {
		return clickPhone;
	}
	/**
	 * 设置：姓名
	 */
	public void setClickName(String clickName) {
		this.clickName = clickName;
	}
	/**
	 * 获取：姓名
	 */
	public String getClickName() {
		return clickName;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：访问ip
	 */
	public void setClickIp(String clickIp) {
		this.clickIp = clickIp;
	}
	/**
	 * 获取：访问ip
	 */
	public String getClickIp() {
		return clickIp;
	}
	/**
	 * 设置：访问地址
	 */
	public void setClickAddress(String clickAddress) {
		this.clickAddress = clickAddress;
	}
	/**
	 * 获取：访问地址
	 */
	public String getClickAddress() {
		return clickAddress;
	}
	/**
	 * 设置：经度
	 */
	public void setClickLongitude(String clickLongitude) {
		this.clickLongitude = clickLongitude;
	}
	/**
	 * 获取：经度
	 */
	public String getClickLongitude() {
		return clickLongitude;
	}
	/**
	 * 设置：维度
	 */
	public void setClickLatitude(String clickLatitude) {
		this.clickLatitude = clickLatitude;
	}
	/**
	 * 获取：维度
	 */
	public String getClickLatitude() {
		return clickLatitude;
	}
	/**
	 * 设置：文章主表id
	 */
	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}
	/**
	 * 获取：文章主表id
	 */
	public Long getArticleId() {
		return articleId;
	}
	/**
	 * 设置：是否有效 0.有效  1.无效
	 */
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	/**
	 * 获取：是否有效 0.有效  1.无效
	 */
	public String getDelFlag() {
		return delFlag;
	}
	/**
	 * 设置：记录状态  1.点击  2.点赞
	 */
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	/**
	 * 获取：记录状态  1.点击  2.点赞
	 */
	public String getRecordType() {
		return recordType;
	}
	/**
	 * 设置：状态  1.未点赞  2.已点赞
	 */
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	/**
	 * 获取：状态  1.未点赞  2.已点赞
	 */
	public String getRecordStatus() {
		return recordStatus;
	}
}
