package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统设置基本配置
 *
 * @author hcf
 * @email 1992lcg@163.com
 * @date 2020-10-28 14:28:36
 */
public class QualificationDO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String qualificationName;
    private Date createTime;
    private String delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQualificationName() {
        return qualificationName;
    }

    public void setQualificationName(String qualificationName) {
        this.qualificationName = qualificationName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }
}
