package com.bootdo.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2022-03-03 10:07:22
 */
public class WordDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//留言人Id
	private Long fromUserId;
	//留言人姓名
	private String fromFullName;
	//留言标题
	private String title;
	//留言内容
	private String content;
	//留言时间
	private Date createDate;
	//回复状态  0未回复  1已回复
	private String replyStatus;
	//回复人用户id
	private Long replyUserId;
	//回复人姓名
	private String replyFullName;
	//回复人内容
	private String replyContent;
	//回复时间
	private Date replyDate;
	//0 有效  1无效
	private String delFlag;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：留言人Id
	 */
	public void setFromUserId(Long fromUserId) {
		this.fromUserId = fromUserId;
	}
	/**
	 * 获取：留言人Id
	 */
	public Long getFromUserId() {
		return fromUserId;
	}
	/**
	 * 设置：留言人姓名
	 */
	public void setFromFullName(String fromFullName) {
		this.fromFullName = fromFullName;
	}
	/**
	 * 获取：留言人姓名
	 */
	public String getFromFullName() {
		return fromFullName;
	}
	/**
	 * 设置：留言标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：留言标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：留言内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：留言内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：留言时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：留言时间
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：回复状态  0未回复  1已回复
	 */
	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}
	/**
	 * 获取：回复状态  0未回复  1已回复
	 */
	public String getReplyStatus() {
		return replyStatus;
	}
	/**
	 * 设置：回复人用户id
	 */
	public void setReplyUserId(Long replyUserId) {
		this.replyUserId = replyUserId;
	}
	/**
	 * 获取：回复人用户id
	 */
	public Long getReplyUserId() {
		return replyUserId;
	}
	/**
	 * 设置：回复人姓名
	 */
	public void setReplyFullName(String replyFullName) {
		this.replyFullName = replyFullName;
	}
	/**
	 * 获取：回复人姓名
	 */
	public String getReplyFullName() {
		return replyFullName;
	}
	/**
	 * 设置：回复人内容
	 */
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	/**
	 * 获取：回复人内容
	 */
	public String getReplyContent() {
		return replyContent;
	}
	/**
	 * 设置：回复时间
	 */
	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}
	/**
	 * 获取：回复时间
	 */
	public Date getReplyDate() {
		return replyDate;
	}
	/**
	 * 设置：0 有效  1无效
	 */
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	/**
	 * 获取：0 有效  1无效
	 */
	public String getDelFlag() {
		return delFlag;
	}
}
