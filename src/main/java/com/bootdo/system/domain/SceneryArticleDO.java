package com.bootdo.system.domain;

import com.bootdo.common.domain.FileDO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统设置基本配置
 *
 * @author hcf
 * @email 1992lcg@163.com
 * @date 2020-10-23 14:28:36
 */
public class SceneryArticleDO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String articleTitle;
    private String attachId;
    private String articleContent;
    private String articleIntroduction;
    private Date createTime;
    private Date updateTime;
    private String articleType;
    private String delFlag;

    private String palyType;
    private String price;
    private String originalPrice;
    private String telphone;

    private List<FileDO> filePathList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public String getArticleIntroduction() {
        return articleIntroduction;
    }

    public void setArticleIntroduction(String articleIntroduction) {
        this.articleIntroduction = articleIntroduction;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public List<FileDO> getFilePathList() {
        return filePathList;
    }

    public void setFilePathList(List<FileDO> filePathList) {
        this.filePathList = filePathList;
    }

    public String getPalyType() {
        return palyType;
    }

    public void setPalyType(String palyType) {
        this.palyType = palyType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }
}
