package com.bootdo.system.dao;

import com.bootdo.system.domain.LinkDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-16 10:15:54
 */
@Mapper
public interface LinkDao {

	LinkDO get(Long id);
	
	List<LinkDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(LinkDO link);
	
	int update(LinkDO link);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
