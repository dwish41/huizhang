package com.bootdo.system.dao;

import com.bootdo.system.domain.ArticleDO;

import java.util.List;
import java.util.Map;

import com.bootdo.system.domain.ClassDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-02 13:46:07
 */
@Mapper
public interface ArticleDao {

	ArticleDO get(Long id);
	
	List<ArticleDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ArticleDO article);
	
	int update(ArticleDO article);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

    List<ClassDO> getSmallClass(Map<String, Object> map);

	void addViewCount(ArticleDO articleDO);

	void addGoodCount(ArticleDO articleDO);
}
