package com.bootdo.system.dao;

import com.bootdo.system.domain.QualificationDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface QualificationDao {

    List<QualificationDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    QualificationDO get(Long id);

    int save(QualificationDO qualificationDO);

    int remove(Long id);

    int batchRemove(Long[] ids);

    int update(QualificationDO qualificationDO);
}
