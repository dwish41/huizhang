package com.bootdo.system.dao;

import com.bootdo.system.domain.RecordDetailDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-01 10:55:13
 */
@Mapper
public interface RecordDetailDao {

	RecordDetailDO get(Long id);
	
	List<RecordDetailDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(RecordDetailDO recordDetail);
	
	int update(RecordDetailDO recordDetail);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
