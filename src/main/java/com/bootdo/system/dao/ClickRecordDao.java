package com.bootdo.system.dao;

import com.bootdo.system.domain.ClickRecordDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-03 11:00:50
 */
@Mapper
public interface ClickRecordDao {

	ClickRecordDO get(Long id);
	
	List<ClickRecordDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ClickRecordDO clickRecord);
	
	int update(ClickRecordDO clickRecord);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
