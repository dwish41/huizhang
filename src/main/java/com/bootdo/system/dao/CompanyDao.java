package com.bootdo.system.dao;

import com.bootdo.system.domain.CompanyDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2022-03-07 14:21:29
 */
@Mapper
public interface CompanyDao {

	CompanyDO get(Long id);

	List<CompanyDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);

	int save(CompanyDO company);

	int update(CompanyDO company);

	int remove(Long id);

	int batchRemove(Long[] ids);
}
