package com.bootdo.system.dao;

import com.bootdo.system.domain.CollectionDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 收藏
 *
 * @author zhouxm
 */
@Mapper
public interface CollectionDao {
    /**
     * 保存
     *
     * @param collectionDO 收藏实体对象
     * @return 大于0表示成功
     * @throws Exception 异常
     */
    int save(CollectionDO collectionDO) throws Exception;

    /**
     * 修改
     *
     * @param collectionDO 收藏实体对象
     * @return 大于0表示成功
     * @throws Exception 异常
     */
    int update(CollectionDO collectionDO) throws Exception;

    /**
     * 根据商品统计收藏数据
     *
     * @return 返回商品以及数量
     * @throws Exception 异常
     */
    List<Map<String, Object>> groupByProduct() throws Exception;
}
