package com.bootdo.system.dao;

import com.bootdo.system.domain.WordDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2022-03-03 10:07:22
 */
@Mapper
public interface WordDao {

	WordDO get(Long id);

	List<WordDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);

	int save(WordDO word);

	int update(WordDO word);

	int remove(Long ID);

	int batchRemove(Long[] ids);
}
