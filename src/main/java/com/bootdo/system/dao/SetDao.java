package com.bootdo.system.dao;

import com.bootdo.system.domain.SetDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SetDao {

    List<SetDO> list(@Param(value="type")String type);

    int update(@Param(value="setCode")String setCode,@Param(value="setValue")String setValue);

    SetDO get(@Param(value="setCode")String setCode);
}
