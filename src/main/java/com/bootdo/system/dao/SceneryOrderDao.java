package com.bootdo.system.dao;

import com.bootdo.system.domain.SceneryOrderDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SceneryOrderDao {

    List<SceneryOrderDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    SceneryOrderDO get(SceneryOrderDO sceneryOrderDO);

    int save(SceneryOrderDO sceneryOrderDO);

    int update(SceneryOrderDO sceneryOrderDO);

}
