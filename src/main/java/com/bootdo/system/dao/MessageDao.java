package com.bootdo.system.dao;

import com.bootdo.system.domain.MessageDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MessageDao {

    List<MessageDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int save(MessageDO messageDO);
}
