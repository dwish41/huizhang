package com.bootdo.system.dao;

import com.bootdo.system.domain.SceneryArticleDO;
import com.bootdo.system.domain.SceneryPlayDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SceneryPlayDao {

    List<SceneryPlayDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int update(SceneryPlayDO sceneryPlayDO);

    SceneryPlayDO get(@Param("id") Long id);
}
