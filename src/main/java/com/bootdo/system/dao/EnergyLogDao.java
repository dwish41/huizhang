package com.bootdo.system.dao;

import com.bootdo.system.domain.EnergyLogDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2021-02-19 09:40:02
 */
@Mapper
public interface EnergyLogDao {

	EnergyLogDO get(Long id);
	
	List<EnergyLogDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(EnergyLogDO log);
	
	int update(EnergyLogDO log);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

    EnergyLogDO getEnergyLogDetail(Map<String, Object> map);
}
