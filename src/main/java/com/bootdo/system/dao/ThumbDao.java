package com.bootdo.system.dao;

import com.bootdo.system.domain.ThumbDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 点赞
 *
 * @author zhouxm
 */
@Mapper
public interface ThumbDao {
    /**
     * 保存
     *
     * @param thumbDO 点赞实体对象
     * @return 大于0表示成功
     * @throws Exception 异常
     */
    int save(ThumbDO thumbDO) throws Exception;

    /**
     * 修改
     *
     * @param thumbDO 点赞实体对象
     * @return 大于0表示成功
     * @throws Exception 异常
     */
    int update(ThumbDO thumbDO) throws Exception;

    /**
     * 根据商品统计点赞数据
     *
     * @return 返回商品以及数量
     * @throws Exception 异常
     */
    List<Map<String, Object>> groupByProduct() throws Exception;
}
