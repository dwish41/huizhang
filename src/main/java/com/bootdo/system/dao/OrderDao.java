package com.bootdo.system.dao;

import com.bootdo.system.domain.OrderDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface OrderDao {

    List<OrderDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    Integer getNum(@Param("ticketTime") String ticketTime);

    int save(OrderDO orderDO);

    OrderDO findByOrderNo(String orderNo);

    int update(OrderDO orderDO);
}
