package com.bootdo.system.dao;

import com.bootdo.system.domain.RecordDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-01 10:55:13
 */
@Mapper
public interface RecordDao {

	RecordDO get(Long id);
	
	List<RecordDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(RecordDO record);
	
	int update(RecordDO record);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

	RecordDO getRecordDetail(RecordDO recordDO);

	void addCount(RecordDO recordDO);
}
