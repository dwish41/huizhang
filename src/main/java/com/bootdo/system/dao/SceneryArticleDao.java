package com.bootdo.system.dao;

import com.bootdo.system.domain.SceneryArticleDO;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SceneryArticleDao {

    List<SceneryArticleDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    SceneryArticleDO get(@Param("id") Long id,@Param("articleType") String articleType,@Param("palyType") String palyType);

    int save(SceneryArticleDO sceneryArticleDO);

    int remove(Long id);

    int batchRemove(Long[] ids);

    int update(SceneryArticleDO sceneryArticleDO);
}
