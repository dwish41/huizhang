package com.bootdo.system.dao;

import com.bootdo.system.domain.UserCodeDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户验证码发送Dao
 *
 * @author zhouxm
 */
@Mapper
public interface UserCodeDao {
    /**
     * 保存短信验证
     *
     * @param userCodeDO 短信验证码发送对象
     * @return 成功返回1
     * @throws Exception 异常
     */
    int save(UserCodeDO userCodeDO) throws Exception;

    /**
     * 判断验证码是否使用
     *
     * @param mobile 手机号
     * @param code   验证码
     * @return 返回未使用的验证码实体
     * @throws Exception 异常
     */
    UserCodeDO getByCodeUsed(String mobile, String code) throws Exception;

    /**
     * 修改验证码使用状态
     *
     * @param userCodeDO 验证码实体
     * @return 大于0表示修改成功
     * @throws Exception 异常
     */
    int updateCodeStatus(UserCodeDO userCodeDO) throws Exception;
}
