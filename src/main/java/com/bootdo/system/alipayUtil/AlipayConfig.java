package com.bootdo.system.alipayUtil;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016110100784886";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCVDTtPlr7s3AqzLqGL1IoSrofXQpQdXRpVjOLbGbOjy/FenFxrUDcsVAf9tPPQ3NiqimVK2l9lmM66b9JLpCFxsTFR5dlKn3piZg4uWf9jaH8DdkSwPplz11niG4jus8c6YxghRIeSw8vceA9e0HAHHTgE/S0H6mk5wY+f4Lx3Pyi1IPBAx8ChApuvCI1BCJBzLOJrIf80BKnMU6eIOXk5d2xpylop4c9EYEpaaaj2N9xjGC0CZKLvUBqOAJ4wy5EZvtFGZHy8uKOrpndZKxx+q7u5DGhqLum6Gw3o4lLdwysNdFjNpsnDbmYoIUpcLdyJl1lnI53eHX3MvZckX8J3AgMBAAECggEAB6qtkVN0sKRE3KCaaKHfFETr8dOX4MbIMvxTMufoiJVV7gLm8hInNF2CJ7emDuk9L9B8k1QWMhihEY/s5j0kB0ARBGBpQ+BeIC+VrSat70QRvHDZ6aLeq8m48tttuLGAnu038YYf3xm5I7b9O2hHSuZi2kbXiR+yl+jv5U5fA1ck//CIhm4S+1G63M/UM+Ab9t2YrQPmvgZb1YXkt3PcbuT2AQWRF6Uvz5pb4tDmr/wMlSysiF9Hs0XYbBLF2dVQrD+s3NVMp5u4kZJ5HsUf52l2gozr398ReyVseuKdCEbGMLbink44WbRAxvxjagtfbKdKTlSOXmsBZDoYhiexmQKBgQDj+ZZKyVzO0+hdBR3FsBcFAywtv1ITJSqvc67JvNPsRnii518hvSyLpFdtUU19ziOePR2siYdtTqguf+81ww9YEbds4x5WEwPLSqPS+dH2+SNMLdAM1/ZAMATjI92PXHDAdtUtVsSXoGEtKVaiEmyrk06Z/aC6fW/MZlp8Yq5newKBgQCnX+pMPNIxtt+alpNLHrQLP+OUMpCuUqjuIOxiqp8y+gcmAgijo3DhwIZbsX1Cjio5CxNbaLPkQHjcfjgk3szQy9tC70xqyi5WuVPY59+DMW/9BcXunEAAef6AaJccIz6i0v1ja0bYTa9l29iGO7i54hr1NTKj99mtgl9m+7IiNQKBgH3x7xPV9s3UyDRLlU0XoUAFpqyHW+Jz0AcqE9Aj37hSnmbxB99t0WQ8IIimN/deoNLU1MXzHLxIjiVSTGcZIir3bqxbNPE8hAYVcDgoB9U3gkadsKjHzqVbYffSYp7G5QuPGup3WxihY849Xj0fjKWlNfFg6yDcF52GlkrNgahtAoGBAJdUG0yuuQUF6QmmFqqWqgZ5z1PQnAtg5SfYUKGszp+SfCAbOqBpR8h0/4Wr5irkyrwkjW3aLruypxQEctKzNs+ZYTcVkSwEfP0gNvUoQGgnrK9CJwFj+GH16utCpU1leF5TEZJwDe/H3UvlB9Hl8VDvY5xMaurhTsiX1ZYQ/6/5AoGAHdTQwnbJz30V5TUWr0at0J/lbrBU2WnpXBrxo0abwX4zxfZRzy8ZAniI/k1gHkUZ0hpxm9Npihov5fqEo23NE0dxWiqLMm3eU1+QreDdSyCzzCzVsRpMszHi1nSsK86h/TVMTwBIrjLcQppCXb6DQKj4ZutDLm6ZLADHK5SAljs=";

   //应用公钥证书路径，下载后保存位置的绝对路径
   //public static String certPath="/usr/local/CSR/appCertPublicKey_2016110100784886.crt";
   public static String certPath="C:\\CSR\\appCertPublicKey_2016110100784886.crt";

    //支付宝公钥证书路径，下载后保存位置的绝对路径
    //public static String alipayPublicCertPath="/usr/local/CSR/alipayCertPublicKey_RSA2.crt";
    public static String alipayPublicCertPath="C:\\CSR\\alipayCertPublicKey_RSA2.crt";

    //支付宝根证书路径，下载后保存位置的绝对路径
    //public static String rootCertPath="/usr/local/CSR/alipayRootCert.crt";
    public static String rootCertPath="C:\\CSR\\alipayRootCert.crt";


    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIDqDCCApCgAwIBAgIQICARE2bHRIUkBU6ag+4MRDANBgkqhkiG9w0BAQsFADCBkTELMAkGA1UE"+
            "BhMCQ04xGzAZBgNVBAoMEkFudCBGaW5hbmNpYWwgdGVzdDElMCMGA1UECwwcQ2VydGlmaWNhdGlv"+
            "biBBdXRob3JpdHkgdGVzdDE+MDwGA1UEAww1QW50IEZpbmFuY2lhbCBDZXJ0aWZpY2F0aW9uIEF1"+
            "dGhvcml0eSBDbGFzcyAyIFIxIHRlc3QwHhcNMjAxMTEzMDgyNzA0WhcNMjMxMTEyMDgyNzA0WjB6"+
            "MQswCQYDVQQGEwJDTjEVMBMGA1UECgwM5rKZ566x546v5aKDMQ8wDQYDVQQLDAZBbGlwYXkxQzBB"+
            "BgNVBAMMOuaUr+S7mOWunSjkuK3lm70p572R57uc5oqA5pyv5pyJ6ZmQ5YWs5Y+4LTIwODgxMDIx"+
            "ODE2MTExODYwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCoOGzu7t1JkIO8ORrUeajj"+
            "6CMnkaLkUrlX8ntpxnM6XXtlLU0Fa4vDmN6Yxez2KpFfhAgksZ87alOgq1vB0mKzJXxP3nJO2gTs"+
            "FP61HDcUgtBqSz0gdWsxkTGBmhX1te0jZHs/qUz1ZihPv/Yed7dMQ/IFs915ohWz/OUh6lDUT++Y"+
            "obaOwdq4VFsVu2IwSmNcOWrgEC696YN+hZmNof8mJRUsI1SYwdB3/NGEBoIjDAjniyEUeYNxEsBK"+
            "m1hE6RiLmBi7XWY4VElTjaMdbc8nFY0wEJMVtRqoQwpppohUD5nVlk2yJ2wr1T6pLfb8QsG9gtkN"+
            "mRl79QhrkC0sZmFFAgMBAAGjEjAQMA4GA1UdDwEB/wQEAwIE8DANBgkqhkiG9w0BAQsFAAOCAQEA"+
            "WErOqpYy9LNglEzs5rNVltCWpbW+X9S79YXbjtidACftfX79oNTJ7b0FOW3t5w841iCGbaqcwIOY"+
            "EVX1+exCKR8o5Ld0hitsHm584GbicY3ZQzbiM5w1CaehAmWb190nqbmArCs89niJl2xGNq4kUTPE"+
            "WZK00drDb8lMswhAWtxWqBVf/clcmufjpMCSGPBh860PPVdnHtt9T9qwDEM+JDrV8zNtFYsqR19j"+
            "Vwsb2fMWKi0PLDj2WgYARXkNqg/0NSeA/L9XQZ6Doq18XIYPZBcL+FbyA9ku7huoyIBcDiXz4yca"+
            "nOkmHHT8/gVVcU7iGWpFYOm+cLvZK0IhQ7BnAQ==";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://hnsbhjq.com/alipay/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://hnsbhjq.com/alipay/return";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
