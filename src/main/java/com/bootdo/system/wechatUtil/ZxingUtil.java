package com.bootdo.system.wechatUtil;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;

public class ZxingUtil {

    public static String enCode(String contents) {
        String base64Code = null;
        int width = 300, height = 300;
        Hashtable<Object, Object> hints = new Hashtable<Object, Object>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        hints.put(EncodeHintType.MARGIN, 1);

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        try {
            BitMatrix byteMatrix = new MultiFormatWriter().encode(new String(contents.getBytes("UTF-8"),"iso-8859-1"), BarcodeFormat.QR_CODE, width, height);
            byteMatrix = deleteWhite(byteMatrix);//删除白边
            MatrixToImageWriter.writeToStream(byteMatrix, "png", bao);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }  catch (WriterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        base64Code =Base64Code(bao.toByteArray());
        return base64Code;
    }

    //删除白边
    private static BitMatrix deleteWhite(BitMatrix matrix) {
        int[] rec = matrix.getEnclosingRectangle();
        int resWidth = rec[2] + 1;
        int resHeight = rec[3] + 1;

        BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);
        resMatrix.clear();
        for (int i = 0; i < resWidth; i++) {
            for (int j = 0; j < resHeight; j++) {
                if (matrix.get(i + rec[0], j + rec[1])){
                    resMatrix.set(i, j);
                }
            }
        }
        return resMatrix;
    }

    public static String  Base64Code(byte[] b) {
        BASE64Encoder encoder = new BASE64Encoder();
        StringBuilder pictureBuffer = new StringBuilder();
        pictureBuffer.append(encoder.encode(b));
        String codeBase64 = pictureBuffer.toString();
        return codeBase64;
    }
}
