package com.bootdo.system.wechatUtil;

import org.jdom2.JDOMException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

import static com.bootdo.system.wechatUtil.MD5Util.MD5Encode;


public class WeChatPay {

    /**
     * 二维码支付
     * @param orderNo
     * @param money
     * @param body
     * @param ip
     * @return
     */
    public Map getPaymentMapCode(String orderNo, String money, String body, String ip){

        String nonce_str=RandomUtil.createRandomString(32);
        //一次签名
        SortedMap<Object, Object> paramMap = new TreeMap<Object, Object>();
        paramMap.put("appid",PaymentConfig.appid);//公众号ID
        paramMap.put("mch_id",PaymentConfig.mch_id);//商户号
        paramMap.put("nonce_str", nonce_str);//32位随机字符串
        paramMap.put("body",body);//商品描述
        paramMap.put("out_trade_no",orderNo);//商户订单号
        paramMap.put("total_fee",money);//设置交易金额 金额为分
        paramMap.put("spbill_create_ip",ip);//客户机IP
        paramMap.put("notify_url",PaymentConfig.wxRetrun);//通知地址
        paramMap.put("trade_type","NATIVE");//支付方式 原生扫码
        paramMap.put("product_id", "shangpingid"); //自行定义

        paramMap.put("sign", SignUtil.createSign(paramMap, PaymentConfig.appKey));

        //沙箱环境测试
        //paramMap.put("sign", SignUtil.createSign(paramMap, getSignKeyUtils.getSignKey(nonce_str,PaymentConfig.mch_id,PaymentConfig.appKey)));

        String rXml = "";
        String prepayid="";
        try {
            rXml = new String(XmlPostUtil.sendXmlRequest(PaymentConfig.pay_url, XMLUtil.mapToXml(paramMap)));
            prepayid = (String) XMLUtil.doXMLParse(rXml).get("prepay_id");//得到预支付id
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //二次签名
        SortedMap<Object, Object> paramMap1 = new TreeMap<Object, Object>();
        paramMap1.put("appId", PaymentConfig.appid);
        paramMap1.put("timeStamp", System.currentTimeMillis());
        paramMap1.put("package", "prepay_id="+prepayid);
        paramMap1.put("signType", "MD5");
        String nonceStr2=RandomUtil.createRandomString(32);
        paramMap1.put("nonceStr",nonceStr2);

        paramMap1.put("paySign", SignUtil.createSign(paramMap1, PaymentConfig.appKey));

        //沙箱环境
        //paramMap1.put("paySign", SignUtil.createSign(paramMap1, getSignKeyUtils.getSignKey(nonceStr2,PaymentConfig.mch_id,PaymentConfig.appKey)));

        try {
            Map map = XMLUtil.doXMLParse(rXml);
            System.out.println("return_code:"+map.get("return_code"));
            System.out.println("code_url:"+map.get("code_url"));
            return map;
        } catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return paramMap1;

    }


    //通知微信正确接收
    public static String getSuccessXml() {
        String xml = "<xml>" +
                "<return_code><![CDATA[SUCCESS]]></return_code>" +
                "<return_msg><![CDATA[OK]]></return_msg>" +
                "</xml>";
        return xml;
    }
}
