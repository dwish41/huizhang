package com.bootdo.system.service;

import com.bootdo.system.domain.SceneryArticleDO;
import com.bootdo.system.domain.SceneryPlayDO;

import java.util.List;
import java.util.Map;

public interface SceneryPlayService {

    List<SceneryPlayDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int update(SceneryPlayDO sceneryPlayDO);

    SceneryPlayDO get(Long id);
}
