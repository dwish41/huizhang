package com.bootdo.system.service;

import com.bootdo.system.domain.ArticleDO;
import com.bootdo.system.domain.ClassDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-02 13:46:07
 */
public interface ArticleService {
	
	ArticleDO get(Long id);
	
	List<ArticleDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ArticleDO article);
	
	int update(ArticleDO article);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

	/**
	 * 根据大类获取小类
	 * @param bigClass
	 * @return
	 */
    List<ClassDO> getSmallClass(String bigClass);

	List<ArticleDO> articleList(Map<String, Object> map);
}
