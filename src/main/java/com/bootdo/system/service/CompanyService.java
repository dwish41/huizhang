package com.bootdo.system.service;

import com.bootdo.system.domain.CompanyDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2022-03-07 14:21:29
 */
public interface CompanyService {
	
	CompanyDO get(Long id);
	
	List<CompanyDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CompanyDO company);
	
	int update(CompanyDO company);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
