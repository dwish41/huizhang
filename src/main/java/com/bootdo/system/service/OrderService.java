package com.bootdo.system.service;

import com.bootdo.system.domain.OrderDO;

import java.util.List;
import java.util.Map;

public interface OrderService {

    List<OrderDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    Integer getNum(String ticketTime);

    int save(OrderDO orderDO);

    OrderDO findByOrderNo(String orderNo);

    int update(OrderDO orderDO);

}
