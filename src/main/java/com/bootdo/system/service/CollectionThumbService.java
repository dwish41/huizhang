package com.bootdo.system.service;

import com.bootdo.common.utils.R;
import com.bootdo.system.domain.CollectionDO;
import com.bootdo.system.domain.ThumbDO;

import java.util.List;
import java.util.Map;

/**
 * 前台收藏与点赞service
 *
 * @author zhouxm
 */
public interface CollectionThumbService {
    /**
     * 保存收藏
     *
     * @param collectionDO 收藏实体对象
     * @return 返回
     * @throws Exception 异常
     */
    int saveCollection(CollectionDO collectionDO) throws Exception;

    /**
     * 修改收藏
     *
     * @param collectionDO 收藏实体对象
     * @return 返回
     * @throws Exception 异常
     */
    int updateCollection(CollectionDO collectionDO) throws Exception;

    /**
     * 保存收藏
     *
     * @param thumbDO 点赞实体对象
     * @return 返回
     * @throws Exception 异常
     */
    int saveThumb(ThumbDO thumbDO) throws Exception;

    /**
     * 修改收藏
     *
     * @param thumbDO 点赞实体对象
     * @return 返回
     * @throws Exception 异常
     */
    int updateThumb(ThumbDO thumbDO) throws Exception;

    /**
     * 根据商品统计收藏数量
     *
     * @return 返回map
     * @throws Exception
     */
    List<Map<String, Object>> groupCollectionByProduct() throws Exception;

    /**
     * 根据商品统计点赞数量
     *
     * @return 返回map
     * @throws Exception
     */
    List<Map<String, Object>> groupThumbByProduct() throws Exception;
}
