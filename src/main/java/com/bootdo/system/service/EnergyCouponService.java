package com.bootdo.system.service;

import com.bootdo.system.domain.EnergyCouponDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2021-02-19 09:51:10
 */
public interface EnergyCouponService {
	
	EnergyCouponDO get(Long id);
	
	List<EnergyCouponDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(EnergyCouponDO coupon);
	
	int update(EnergyCouponDO coupon);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

    EnergyCouponDO getEnergyCouponDetail(Map<String, Object> map);
}
