package com.bootdo.system.service;

import com.bootdo.common.pojo.UserReq;
import com.bootdo.system.domain.UserDO;

/**
 * 前台登陆service
 *
 * @author zhouxm
 */
public interface LoginService {
    /**
     * 短信验证码发送
     *
     * @param mobile 手机号
     * @return 返回验证码
     * @throws Exception 异常
     */
    String sendVerifyCode(String mobile) throws Exception;

    /**
     * 判断验证码是否使用
     *
     * @param mobile 手机号
     * @param code   验证码
     * @return true表示使用
     * @throws Exception 异常
     */
    boolean checkCodeUsed(String mobile, String code) throws Exception;

    /**
     * 修改验证码使用状态
     *
     * @param mobile 手机号
     * @param code   验证码
     * @return 大于0表示修改成功
     * @throws Exception 异常
     */
    int updateCodeStatus(String mobile, String code) throws Exception;

    /**
     * 注册
     *
     * @param req 用户信息参数
     * @return 大于0表示注册成功
     * @throws Exception 异常
     */
    int register(UserReq req) throws Exception;

    /**
     * 用户能量值增加
     * @param userDO
     */
    void addEnergy(UserDO userDO);
}
