package com.bootdo.system.service;

import com.bootdo.system.domain.ClassDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-16 09:24:39
 */
public interface ClassService {
	
	ClassDO get(Long id);
	
	List<ClassDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ClassDO classDO);
	
	int update(ClassDO classDO);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
