package com.bootdo.system.service;

import com.bootdo.system.domain.ClickRecordDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-09-03 11:00:50
 */
public interface ClickRecordService {
	
	ClickRecordDO get(Long id);
	
	List<ClickRecordDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ClickRecordDO clickRecord);
	
	int update(ClickRecordDO clickRecord);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
