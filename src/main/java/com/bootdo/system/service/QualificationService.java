package com.bootdo.system.service;

import com.bootdo.system.domain.QualificationDO;

import java.util.List;
import java.util.Map;

public interface QualificationService {

    List<QualificationDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    QualificationDO get(Long id);

    int save(QualificationDO qualificationDO);

    int remove(Long id);

    int batchRemove(Long[] ids);

    int update(QualificationDO qualificationDO);
}
