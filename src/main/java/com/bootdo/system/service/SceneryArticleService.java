package com.bootdo.system.service;

import com.bootdo.system.domain.ArticleDO;
import com.bootdo.system.domain.SceneryArticleDO;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author hcf
 * @email 1992lcg@163.com
 * @date 2020-10-23 13:46:07
 */
public interface SceneryArticleService {

    List<SceneryArticleDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    SceneryArticleDO get(Long id,String articleType);

    int save(SceneryArticleDO sceneryArticleDO);

    int remove(Long id);

    int batchRemove(Long[] ids);

    int update(SceneryArticleDO sceneryArticleDO);
}
