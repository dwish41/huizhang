package com.bootdo.system.service;

import com.bootdo.system.domain.SetDO;

import java.util.List;

public interface SetService {

    List<SetDO> list(String type);

    int updateCompany(String companyName,String copyright,String recordNo,String technicalSupport);

    int updatePhone(String consultPhone,String complainPhone,String faxPhone);

    int updateWeChatNum(String weChatNum);

    SetDO get(String setCode);
}
