package com.bootdo.system.service;

import com.bootdo.system.domain.MessageDO;

import java.util.List;
import java.util.Map;

public interface MessageService {

    List<MessageDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int save(MessageDO messageDO);
}
