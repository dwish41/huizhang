package com.bootdo.system.service.impl;

import com.bootdo.system.dao.QualificationDao;
import com.bootdo.system.domain.QualificationDO;
import com.bootdo.system.service.QualificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class QualificationServiceImpl implements QualificationService {

    @Autowired
    private QualificationDao qualificationDao;

    @Override
    public List<QualificationDO> list(Map<String, Object> map) {
        return qualificationDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return qualificationDao.count(map);
    }


    @Override
    public QualificationDO get(Long id){
        return qualificationDao.get(id);
    }

    @Override
    public int save(QualificationDO qualificationDO){
        qualificationDO.setCreateTime(new Date());
        return qualificationDao.save(qualificationDO);
    }

    @Override
    public int remove(Long id){
        return qualificationDao.remove(id);
    }

    @Override
    public int batchRemove(Long[] ids){
        return qualificationDao.batchRemove(ids);
    }

    @Override
    public int update(QualificationDO qualificationDO){
        return qualificationDao.update(qualificationDO);
    }
}
