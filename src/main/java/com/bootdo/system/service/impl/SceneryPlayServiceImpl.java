package com.bootdo.system.service.impl;

import com.bootdo.system.dao.SceneryPlayDao;
import com.bootdo.system.domain.SceneryArticleDO;
import com.bootdo.system.domain.SceneryPlayDO;
import com.bootdo.system.service.SceneryPlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SceneryPlayServiceImpl implements SceneryPlayService {

    @Autowired
    private SceneryPlayDao sceneryPlayDao;

    @Override
    public List<SceneryPlayDO> list(Map<String, Object> map) {
        return sceneryPlayDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return sceneryPlayDao.count(map);
    }

    @Override
    public int update(SceneryPlayDO sceneryPlayDO){
        return sceneryPlayDao.update(sceneryPlayDO);
    }

    @Override
    public SceneryPlayDO get(Long id) {
        return sceneryPlayDao.get(id);
    }
}
