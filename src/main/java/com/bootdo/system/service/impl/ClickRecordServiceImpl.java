package com.bootdo.system.service.impl;

import com.bootdo.common.utils.StringUtils;
import com.bootdo.system.dao.ArticleDao;
import com.bootdo.system.domain.ArticleDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bootdo.system.dao.ClickRecordDao;
import com.bootdo.system.domain.ClickRecordDO;
import com.bootdo.system.service.ClickRecordService;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ClickRecordServiceImpl implements ClickRecordService {

	@Autowired
	private ClickRecordDao clickRecordDao;

	@Autowired
	private ArticleDao articleDao;
	
	@Override
	public ClickRecordDO get(Long id){
		return clickRecordDao.get(id);
	}
	
	@Override
	public List<ClickRecordDO> list(Map<String, Object> map){
		return clickRecordDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return clickRecordDao.count(map);
	}
	
	@Override
	public int save(ClickRecordDO clickRecord){
		int count= 0;
		clickRecord.setCreateTime(new Date());
		count = clickRecordDao.save(clickRecord);

		//根据articleId获取详情
		ArticleDO articleDO = articleDao.get(clickRecord.getArticleId());
		if(null != articleDO) {
			//点击量增加
			if(StringUtils.equals("1",clickRecord.getRecordType())) {
				articleDao.addViewCount(articleDO);
			}else if(StringUtils.equals("2",clickRecord.getRecordType())) {
				//点赞量增加
				articleDao.addGoodCount(articleDO);
			}
		}
		return count;
	}
	
	@Override
	public int update(ClickRecordDO clickRecord){
		return clickRecordDao.update(clickRecord);
	}
	
	@Override
	public int remove(Long id){
		return clickRecordDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return clickRecordDao.batchRemove(ids);
	}
	
}
