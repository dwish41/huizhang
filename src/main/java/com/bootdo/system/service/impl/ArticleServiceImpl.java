package com.bootdo.system.service.impl;

import com.bootdo.common.dao.FileDao;
import com.bootdo.common.utils.StringUtils;
import com.bootdo.system.domain.ClassDO;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bootdo.system.dao.ArticleDao;
import com.bootdo.system.domain.ArticleDO;
import com.bootdo.system.service.ArticleService;



@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private ArticleDao articleDao;

	@Autowired
	private FileDao fileDao;

	
	@Override
	public ArticleDO get(Long id){
		ArticleDO articleDO = articleDao.get(id);
		if(StringUtils.isNotEmpty(articleDO.getAttachId())) {
			Map<String,Object> map1 = new HashMap<String, Object>();
			map1.put("attachId",articleDO.getAttachId());
			articleDO.setFilePathList(fileDao.list(map1));
		}
		return articleDO;
	}
	
	@Override
	public List<ArticleDO> list(Map<String, Object> map){
		return articleDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return articleDao.count(map);
	}
	
	@Override
	public int save(ArticleDO article){
		article.setCreateDate(new Date());
		article.setUpdateDate(new Date());
		article.setDelFlag("0");
		article.setViewCount(0l);
		article.setGoodCount(0l);
		return articleDao.save(article);
	}
	
	@Override
	public int update(ArticleDO article){
		article.setUpdateDate(new Date());
		return articleDao.update(article);
	}
	
	@Override
	public int remove(Long id){
		return articleDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return articleDao.batchRemove(ids);
	}

	/***
	 * 根据大类获取小类
	 * @param bigClass
	 * @return
	 */
	@Override
	public List<ClassDO> getSmallClass(String bigClass) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("bigClass",bigClass);
		List<ClassDO> list = articleDao.getSmallClass(map);
		return list;
	}

	@Override
	public List<ArticleDO> articleList(Map<String, Object> map) {
		List<ArticleDO> list = articleDao.list(map);
		if(!CollectionUtils.isEmpty(list)) {
			Map<String,Object> map1 = null;
			for(ArticleDO data : list) {
				if(StringUtils.isNotEmpty(data.getAttachId())) {
					map1 = new HashMap<String, Object>();
					map1.put("attachId",data.getAttachId());
					data.setFilePathList(fileDao.list(map1));
				}
			}
		}
		return list;
	}

}
