package com.bootdo.system.service.impl;

import com.bootdo.system.dao.OrderDao;
import com.bootdo.system.domain.OrderDO;
import com.bootdo.system.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;


    @Override
    public List<OrderDO> list(Map<String, Object> map) {
        return orderDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return orderDao.count(map);
    }

    @Override
    public Integer getNum(String ticketTime){
        return orderDao.getNum(ticketTime);
    }

    @Override
    public int save(OrderDO orderDO){
        orderDO.setCreateTime(new Date());
        return orderDao.save(orderDO);
    }

    @Override
    public OrderDO findByOrderNo(String orderNo){
        return orderDao.findByOrderNo(orderNo);
    }

    @Override
    public int update(OrderDO orderDO){
        return orderDao.update(orderDO);
    }
}
