package com.bootdo.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bootdo.system.dao.WordDao;
import com.bootdo.system.domain.WordDO;
import com.bootdo.system.service.WordService;



@Service
public class WordServiceImpl implements WordService {
	@Autowired
	private WordDao wordDao;

	@Override
	public WordDO get(Long id){
		return wordDao.get(id);
	}

	@Override
	public List<WordDO> list(Map<String, Object> map){
		return wordDao.list(map);
	}

	@Override
	public int count(Map<String, Object> map){
		return wordDao.count(map);
	}

	@Override
	public int save(WordDO word){
		word.setCreateDate(new Date());
		word.setReplyStatus("0");//未回复  1已回复
		word.setDelFlag("0");//0 有效  1无效
		return wordDao.save(word);
	}

	@Override
	public int update(WordDO word){
		word.setReplyDate(new Date());
		word.setReplyStatus("1");//未回复  1已回复
		return wordDao.update(word);
	}

	@Override
	public int remove(Long id){
		return wordDao.remove(id);
	}

	@Override
	public int batchRemove(Long[] ids){
		return wordDao.batchRemove(ids);
	}

}
