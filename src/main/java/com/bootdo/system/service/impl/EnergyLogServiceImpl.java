package com.bootdo.system.service.impl;

import com.bootdo.system.dao.EnergyLogDao;
import com.bootdo.system.domain.EnergyLogDO;
import com.bootdo.system.domain.UserDO;
import com.bootdo.system.service.EnergyLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EnergyLogServiceImpl implements EnergyLogService {

	@Autowired
	private EnergyLogDao energyLogDao;

	@Override
	public EnergyLogDO get(Long id){
		return energyLogDao.get(id);
	}

	@Override
	public List<EnergyLogDO> list(Map<String, Object> map){
		return energyLogDao.list(map);
	}

	@Override
	public int count(Map<String, Object> map){
		return energyLogDao.count(map);
	}

	@Override
	public int save(EnergyLogDO energyLogDO){
		return energyLogDao.save(energyLogDO);
	}

	@Override
	public int update(EnergyLogDO energyLogDO){
		return energyLogDao.update(energyLogDO);
	}
	
	@Override
	public int remove(Long id){
		return energyLogDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return energyLogDao.batchRemove(ids);
	}

	/**
	 * 获取能量值详情
	 * @param map
	 * @return
	 */
	@Override
	public EnergyLogDO getEnergyLogDetail(Map<String, Object> map) {
		return energyLogDao.getEnergyLogDetail(map);
	}

}
