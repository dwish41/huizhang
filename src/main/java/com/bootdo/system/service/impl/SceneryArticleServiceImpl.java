package com.bootdo.system.service.impl;

import com.bootdo.common.dao.FileDao;
import com.bootdo.common.utils.StringUtils;
import com.bootdo.system.dao.SceneryArticleDao;
import com.bootdo.system.domain.ArticleDO;
import com.bootdo.system.domain.SceneryArticleDO;
import com.bootdo.system.service.SceneryArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SceneryArticleServiceImpl implements SceneryArticleService {


    @Autowired
    private SceneryArticleDao sceneryArticleDao;

    @Autowired
    private FileDao fileDao;

    @Override
    public List<SceneryArticleDO> list(Map<String, Object> map) {
        return sceneryArticleDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return sceneryArticleDao.count(map);
    }

    @Override
    public SceneryArticleDO get(Long id,String articleType){
        SceneryArticleDO sceneryArticleDO=sceneryArticleDao.get(id,articleType,null);
        if(sceneryArticleDO!=null) {
            if(StringUtils.isNotEmpty(sceneryArticleDO.getAttachId())) {
                Map<String,Object> map = new HashMap<String, Object>();
                map.put("attachId",sceneryArticleDO.getAttachId());
                sceneryArticleDO.setFilePathList(fileDao.list(map));
            }
        }
        return sceneryArticleDO;
    }

    @Override
    public int save(SceneryArticleDO sceneryArticleDO){
        sceneryArticleDO.setCreateTime(new Date());
        sceneryArticleDO.setUpdateTime(new Date());
        return sceneryArticleDao.save(sceneryArticleDO);
    }

    @Override
    public int remove(Long id){
        return sceneryArticleDao.remove(id);
    }

    @Override
    public int batchRemove(Long[] ids){
        return sceneryArticleDao.batchRemove(ids);
    }

    @Override
    public int update(SceneryArticleDO sceneryArticleDO){
        sceneryArticleDO.setUpdateTime(new Date());
        return sceneryArticleDao.update(sceneryArticleDO);
    }
}
