package com.bootdo.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.bootdo.system.dao.CompanyDao;
import com.bootdo.system.domain.CompanyDO;
import com.bootdo.system.service.CompanyService;



@Service
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private CompanyDao companyDao;

	@Override
	public CompanyDO get(Long id){
		return companyDao.get(id);
	}

	@Override
	public List<CompanyDO> list(Map<String, Object> map){
		return companyDao.list(map);
	}

	@Override
	public int count(Map<String, Object> map){
		return companyDao.count(map);
	}

	@Override
	public int save(CompanyDO company){
		return companyDao.save(company);
	}

	@Override
	public int update(CompanyDO company){
		return companyDao.update(company);
	}

	@Override
	public int remove(Long id){
		CompanyDO companyDO = new CompanyDO();
		companyDO.setId(id);
		companyDO.setDelFlag("1");//w无效
		return companyDao.update(companyDO);
	}

	@Override
	public int batchRemove(Long[] ids){
		int count = 0;
		if(ids.length > 0) {
			CompanyDO companyDO = new CompanyDO();
			companyDO.setDelFlag("1");//w无效
			for (Long id : ids ) {
				companyDO.setId(id);
				count += companyDao.update(companyDO);
			}
		}
		return count;
	}

}
