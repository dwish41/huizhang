package com.bootdo.system.service.impl;

import com.bootdo.system.dao.SceneryOrderDao;
import com.bootdo.system.domain.SceneryOrderDO;
import com.bootdo.system.service.SceneryOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SceneryOrderServiceImpl implements SceneryOrderService {

    @Autowired
    private SceneryOrderDao sceneryOrderDao;

    @Override
    public List<SceneryOrderDO> list(Map<String, Object> map) {
        return sceneryOrderDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return sceneryOrderDao.count(map);
    }

    @Override
    public SceneryOrderDO get(SceneryOrderDO sceneryOrderDO) {
        return sceneryOrderDao.get(sceneryOrderDO);
    }

    @Override
    public int save(SceneryOrderDO sceneryOrderDO) {
        return sceneryOrderDao.save(sceneryOrderDO);
    }

    @Override
    public int update(SceneryOrderDO sceneryOrderDO) {
        return sceneryOrderDao.update(sceneryOrderDO);
    }
}
