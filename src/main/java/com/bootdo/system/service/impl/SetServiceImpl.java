package com.bootdo.system.service.impl;

import com.bootdo.common.utils.StringUtils;
import com.bootdo.system.dao.SetDao;
import com.bootdo.system.domain.SetDO;
import com.bootdo.system.service.SetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SetServiceImpl implements SetService {

    @Autowired
    private SetDao setDaoMapper;

    @Override
    public List<SetDO> list(String type) {
        return setDaoMapper.list(type);
    }

    @Override
    public  int updateCompany(String companyName,String copyright,String recordNo,String technicalSupport){
        setDaoMapper.update("companyName",companyName);
        setDaoMapper.update("copyright",copyright);
        setDaoMapper.update("recordNo",recordNo);
        setDaoMapper.update("technicalSupport",technicalSupport);
        return 1;
    }

    @Override
    public int updatePhone(String consultPhone,String complainPhone,String faxPhone){
        setDaoMapper.update("consultPhone",consultPhone);
        setDaoMapper.update("complainPhone",complainPhone);
        setDaoMapper.update("faxPhone",faxPhone);
        return 1;
    }

    @Override
    public int updateWeChatNum(String weChatNum){
        setDaoMapper.update("weChatNum",weChatNum);
        return 1;
    }

    @Override
    public  SetDO get(String setCode){
        return setDaoMapper.get(setCode);
    }

}
