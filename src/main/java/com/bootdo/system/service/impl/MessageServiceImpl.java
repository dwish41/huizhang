package com.bootdo.system.service.impl;

import com.bootdo.system.dao.MessageDao;
import com.bootdo.system.domain.MessageDO;
import com.bootdo.system.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Override
    public List<MessageDO> list(Map<String, Object> map) {
        return messageDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return messageDao.count(map);
    }

    @Override
    public int save(MessageDO messageDO){
        messageDO.setMessageCreateTime(new Date());
        return messageDao.save(messageDO);
    }
}
