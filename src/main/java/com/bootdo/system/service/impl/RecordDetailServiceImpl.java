package com.bootdo.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.bootdo.system.dao.RecordDetailDao;
import com.bootdo.system.domain.RecordDetailDO;
import com.bootdo.system.service.RecordDetailService;



@Service
public class RecordDetailServiceImpl implements RecordDetailService {
	@Autowired
	private RecordDetailDao recordDetailDao;
	
	@Override
	public RecordDetailDO get(Long id){
		return recordDetailDao.get(id);
	}
	
	@Override
	public List<RecordDetailDO> list(Map<String, Object> map){
		return recordDetailDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return recordDetailDao.count(map);
	}
	
	@Override
	public int save(RecordDetailDO recordDetail){
		return recordDetailDao.save(recordDetail);
	}
	
	@Override
	public int update(RecordDetailDO recordDetail){
		return recordDetailDao.update(recordDetail);
	}
	
	@Override
	public int remove(Long id){
		return recordDetailDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return recordDetailDao.batchRemove(ids);
	}
	
}
