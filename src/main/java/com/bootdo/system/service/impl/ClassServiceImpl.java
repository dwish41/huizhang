package com.bootdo.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bootdo.system.dao.ClassDao;
import com.bootdo.system.domain.ClassDO;
import com.bootdo.system.service.ClassService;



@Service
public class ClassServiceImpl implements ClassService {
	@Autowired
	private ClassDao classDao;
	
	@Override
	public ClassDO get(Long id){
		return classDao.get(id);
	}
	
	@Override
	public List<ClassDO> list(Map<String, Object> map){
		return classDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return classDao.count(map);
	}
	
	@Override
	public int save(ClassDO classDO){
		classDO.setCreateDate(new Date());
		classDO.setDelFlag("0");
		return classDao.save(classDO);
	}
	
	@Override
	public int update(ClassDO classDO){
		return classDao.update(classDO);
	}
	
	@Override
	public int remove(Long id){
		return classDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return classDao.batchRemove(ids);
	}
	
}
