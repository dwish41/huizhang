package com.bootdo.system.service.impl;

import cn.hutool.core.util.IdUtil;
import com.bootdo.common.config.Constant;
import com.bootdo.common.pojo.UserReq;
import com.bootdo.common.utils.MathUtils;
import com.bootdo.common.utils.RedisUtils;
import com.bootdo.common.utils.SmsSendUtil;
import com.bootdo.system.dao.UserCodeDao;
import com.bootdo.system.dao.UserDao;
import com.bootdo.system.domain.UserCodeDO;
import com.bootdo.system.domain.UserDO;
import com.bootdo.system.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 前台登陆service
 *
 * @author zhouxm
 */
@Service
@Slf4j
public class LoginServiceImpl implements LoginService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private UserCodeDao userCodeDao;

    @Autowired
    private RedisUtils redisUtils;

    private UserCodeDO setUserCode(String mobile, String code) {
        Date date = new Date();
        UserCodeDO userCodeDO = new UserCodeDO();
        userCodeDO.setId(IdUtil.fastSimpleUUID());
        userCodeDO.setCode(code);
        userCodeDO.setMobile(mobile);
        userCodeDO.setSendTime(date);
        userCodeDO.setCreateTime(date);
        return userCodeDO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String sendVerifyCode(String mobile) throws Exception {
        String code = MathUtils.getVerifyCode();
        SmsSendUtil.sendSms(mobile,code);
        log.info("verify code: {}", code);
        /*DictDO dictDO = DictUtils.getDictByType(Constant.VERIFY_CODE_KEY);
        Long timeout = Constant.REDIS_CODE_TIMEOUT;
        if (dictDO != null) {
            timeout = Long.valueOf(dictDO.getValue());
        }*/
        boolean flag = redisUtils.set(mobile, code, 5L, TimeUnit.MINUTES);
        if (flag) {
            userCodeDao.save(setUserCode(mobile, code));
            return code;
        }
        return null;
    }

    @Override
    public boolean checkCodeUsed(String mobile, String code) throws Exception {
        UserCodeDO userCodeDO = userCodeDao.getByCodeUsed(mobile, code);
        return userCodeDO != null;
    }

    @Override
    public int updateCodeStatus(String mobile, String code) throws Exception {
        UserCodeDO userCodeDO = new UserCodeDO();
        userCodeDO.setMobile(mobile);
        userCodeDO.setCode(code);
        userCodeDO.setUpdateTime(new Date());
        userCodeDO.setIsUsed(Constant.CODE_USED);
        return userCodeDao.updateCodeStatus(userCodeDO);
    }

    @Override
    public int register(UserReq req) throws Exception {
        String mobile = req.getMobile();
        UserDO userDO = userDao.getByMobile(mobile);
        if (userDO != null) {
            // 表示用户已经存在
            return -1;
        }
        userDO = setUserData(req);
        return userDao.save(userDO);
    }

    /**
     * 用户能量值增加
     * @param userDO
     */
    @Override
    public void addEnergy(UserDO userDO) {
        userDao.addEnergy(userDO);
    }

    private UserDO setUserData(UserReq req) {
        UserDO userDO = new UserDO();
        userDO.setUsername(req.getUsername());
        userDO.setMobile(req.getMobile());
        userDO.setPassword(req.getPassword());
        userDO.setStatus(1);
        userDO.setGmtCreate(new Date());
        return userDO;
    }
}
