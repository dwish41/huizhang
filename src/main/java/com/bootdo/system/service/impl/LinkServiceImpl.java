package com.bootdo.system.service.impl;

import com.bootdo.system.dao.LinkDao;
import com.bootdo.system.domain.LinkDO;
import com.bootdo.system.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class LinkServiceImpl implements LinkService {
	@Autowired
	private LinkDao linkDao;
	
	@Override
	public LinkDO get(Long id){
		return linkDao.get(id);
	}
	
	@Override
	public List<LinkDO> list(Map<String, Object> map){
		return linkDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return linkDao.count(map);
	}
	
	@Override
	public int save(LinkDO link){
		link.setCreateDate(new Date());
		link.setDelFlag("0");
		return linkDao.save(link);
	}
	
	@Override
	public int update(LinkDO link){
		return linkDao.update(link);
	}
	
	@Override
	public int remove(Long id){
		return linkDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return linkDao.batchRemove(ids);
	}
	
}
