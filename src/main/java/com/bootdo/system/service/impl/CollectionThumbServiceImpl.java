package com.bootdo.system.service.impl;

import cn.hutool.core.util.IdUtil;
import com.bootdo.common.config.Constant;
import com.bootdo.common.utils.R;
import com.bootdo.common.utils.ShiroUtils;
import com.bootdo.system.dao.CollectionDao;
import com.bootdo.system.dao.ThumbDao;
import com.bootdo.system.domain.CollectionDO;
import com.bootdo.system.domain.ThumbDO;
import com.bootdo.system.service.CollectionThumbService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 前台收藏与点赞service
 *
 * @author zhouxm
 */
@Service
@Slf4j
public class CollectionThumbServiceImpl implements CollectionThumbService {
    @Autowired
    private CollectionDao collectionDao;

    @Autowired
    private ThumbDao thumbDao;

    @Override
    public int saveCollection(CollectionDO collectionDO) throws Exception {
        Date date = new Date();
        collectionDO.setId(IdUtil.fastSimpleUUID());
        collectionDO.setGmtCreate(date);
        collectionDO.setEnable(Constant.COLLECTION_THUMB_ENABLE);
        collectionDO.setUserId(ShiroUtils.getUserId());
        return collectionDao.save(collectionDO);
    }

    @Override
    public int updateCollection(CollectionDO collectionDO) throws Exception {
        Date date = new Date();
        collectionDO.setGmtModified(date);
        return collectionDao.update(collectionDO);
    }

    @Override
    public int saveThumb(ThumbDO thumbDO) throws Exception {
        Date date = new Date();
        thumbDO.setId(IdUtil.fastSimpleUUID());
        thumbDO.setGmtCreate(date);
        thumbDO.setUserId(ShiroUtils.getUserId());
        thumbDO.setEnable(Constant.COLLECTION_THUMB_ENABLE);
        return thumbDao.save(thumbDO);
    }

    @Override
    public int updateThumb(ThumbDO thumbDO) throws Exception {
        Date date = new Date();
        thumbDO.setGmtModified(date);
        return thumbDao.update(thumbDO);
    }

    @Override
    public List<Map<String, Object>> groupCollectionByProduct() throws Exception {
        return collectionDao.groupByProduct();
    }

    @Override
    public List<Map<String, Object>> groupThumbByProduct() throws Exception {
        return thumbDao.groupByProduct();
    }
}
