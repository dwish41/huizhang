package com.bootdo.system.service.impl;

import com.bootdo.system.dao.EnergyCouponDao;
import com.bootdo.system.domain.EnergyCouponDO;
import com.bootdo.system.service.EnergyCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class EnergyCouponServiceImpl implements EnergyCouponService {

	@Autowired
	private EnergyCouponDao energyCouponDao;
	
	@Override
	public EnergyCouponDO get(Long id){
		return energyCouponDao.get(id);
	}
	
	@Override
	public List<EnergyCouponDO> list(Map<String, Object> map){
		return energyCouponDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return energyCouponDao.count(map);
	}
	
	@Override
	public int save(EnergyCouponDO coupon){
		return energyCouponDao.save(coupon);
	}
	
	@Override
	public int update(EnergyCouponDO coupon){
		return energyCouponDao.update(coupon);
	}
	
	@Override
	public int remove(Long id){
		return energyCouponDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return energyCouponDao.batchRemove(ids);
	}

	@Override
	public EnergyCouponDO getEnergyCouponDetail(Map<String, Object> map) {
		return energyCouponDao.getEnergyCouponDetail(map);
	}

}
