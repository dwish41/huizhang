package com.bootdo.system.service;

import com.bootdo.system.domain.SceneryOrderDO;

import java.util.List;
import java.util.Map;

public interface SceneryOrderService {

    List<SceneryOrderDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);


    SceneryOrderDO get(SceneryOrderDO sceneryOrderDO);

    int save(SceneryOrderDO sceneryOrderDO);

    int update(SceneryOrderDO sceneryOrderDO);

}
