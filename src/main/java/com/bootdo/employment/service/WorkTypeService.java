package com.bootdo.employment.service;

import com.bootdo.employment.domain.WorkTypeDO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface WorkTypeService {

    List<WorkTypeDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    WorkTypeDO get(Long id);

    int save(WorkTypeDO workType);

    int update(WorkTypeDO workType);

    int remove(Long id);

    int batchremove(Long[] ids);

    boolean exit(Map<String, Object> params);
}
