package com.bootdo.employment.service;

import com.bootdo.employment.domain.EmpDemandDO;
import com.bootdo.employment.domain.WorkTypeDO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface EmpDemandService {

    List<EmpDemandDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    EmpDemandDO get(Long id);

    int save(EmpDemandDO workType);

    int update(EmpDemandDO workType);

    int remove(Long id);

    int batchremove(Long[] ids);

    boolean exit(Map<String, Object> params);
}
