package com.bootdo.employment.service.impl;

import com.bootdo.employment.dao.WorkTypeDao;
import com.bootdo.employment.domain.WorkTypeDO;
import com.bootdo.employment.service.WorkTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Transactional
@Service
public class WorkTypeServiceImpl implements WorkTypeService {
    private static final Logger logger = LoggerFactory.getLogger(WorkTypeService.class);

    @Autowired
    WorkTypeDao workTypeMapper;

    @Override
    public List<WorkTypeDO> list(Map<String, Object> map) {
        return workTypeMapper.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return workTypeMapper.count(map);
    }

    @Transactional
    @Override
    public int save(WorkTypeDO workType) {
        return workTypeMapper.save(workType);
    }

    @Override
    public int update(WorkTypeDO workType) {
        return workTypeMapper.update(workType);
    }

    @Override
    public int remove(Long id) {
        return workTypeMapper.remove(id);
    }

    @Override
    public boolean exit(Map<String, Object> params) {
        boolean exit;
        exit = workTypeMapper.list(params).size() > 0;
        return exit;
    }

    @Transactional
    @Override
    public int batchremove(Long[] ids) {
        int count = workTypeMapper.batchRemove(ids);
        return count;
    }

    @Override
    public WorkTypeDO get(Long id) {
        return workTypeMapper.get(id);
    }
}
