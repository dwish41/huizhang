package com.bootdo.employment.service.impl;

import com.bootdo.employment.dao.EmpDemandDao;
import com.bootdo.employment.domain.EmpDemandDO;
import com.bootdo.employment.service.EmpDemandService;
import com.bootdo.employment.service.WorkTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Transactional
@Service
public class EmpDemandServiceImpl implements EmpDemandService {
    private static final Logger logger = LoggerFactory.getLogger(EmpDemandService.class);

    @Autowired
    EmpDemandDao empDemandMapper;

    @Override
    public List<EmpDemandDO> list(Map<String, Object> map) {
        return empDemandMapper.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return empDemandMapper.count(map);
    }

    @Transactional
    @Override
    public int save(EmpDemandDO workType) {
        return empDemandMapper.save(workType);
    }

    @Override
    public int update(EmpDemandDO workType) {
        return empDemandMapper.update(workType);
    }

    @Override
    public int remove(Long id) {
        return empDemandMapper.remove(id);
    }

    @Override
    public boolean exit(Map<String, Object> params) {
        boolean exit;
        exit = empDemandMapper.list(params).size() > 0;
        return exit;
    }

    @Transactional
    @Override
    public int batchremove(Long[] ids) {
        int count = empDemandMapper.batchRemove(ids);
        return count;
    }

    @Override
    public EmpDemandDO get(Long id) {
        return empDemandMapper.get(id);
    }
}
