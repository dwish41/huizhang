package com.bootdo.employment.dao;

import com.bootdo.employment.domain.WorkTypeDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface WorkTypeDao {

	List<WorkTypeDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);

	int save(WorkTypeDO workType);

	int update(WorkTypeDO workType);

	int remove(Long id);

	int batchRemove(Long[] ids);

	WorkTypeDO get(Long id);
}
