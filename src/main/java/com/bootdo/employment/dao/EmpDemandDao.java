package com.bootdo.employment.dao;

import com.bootdo.employment.domain.EmpDemandDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EmpDemandDao {

	List<EmpDemandDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);

	int save(EmpDemandDO workType);

	int update(EmpDemandDO workType);

	int remove(Long id);

	int batchRemove(Long[] ids);

	EmpDemandDO get(Long id);
}
