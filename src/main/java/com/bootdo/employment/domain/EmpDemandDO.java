package com.bootdo.employment.domain;

import com.bootdo.common.domain.FileDO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用工需求实体类
 */
public class EmpDemandDO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String address;
    private String attachId;
    private String remark;
    private Integer status;
    private Long userIdCreate;
    private Date gmtCreate;
    private Date gmtModified;
    private List<EmpDemandWorkTypeDO> empDemandWorkTypeDOList;
    private List<FileDO> filePathList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getUserIdCreate() {
        return userIdCreate;
    }

    public void setUserIdCreate(Long userIdCreate) {
        this.userIdCreate = userIdCreate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public List<EmpDemandWorkTypeDO> getEmpDemandWorkTypeDOList() {
        return empDemandWorkTypeDOList;
    }

    public void setEmpDemandWorkTypeDOList(List<EmpDemandWorkTypeDO> empDemandWorkTypeDOList) {
        this.empDemandWorkTypeDOList = empDemandWorkTypeDOList;
    }

    public List<FileDO> getFilePathList() {
        return filePathList;
    }

    public void setFilePathList(List<FileDO> filePathList) {
        this.filePathList = filePathList;
    }

    @Override
    public String toString() {
        return "EmpDemandDO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", attachId='" + attachId + '\'' +
                ", remark='" + remark + '\'' +
                ", status=" + status +
                ", userIdCreate=" + userIdCreate +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", empDemandWorkTypeDOList=" + empDemandWorkTypeDOList +
                ", filePathList=" + filePathList +
                '}';
    }
}
