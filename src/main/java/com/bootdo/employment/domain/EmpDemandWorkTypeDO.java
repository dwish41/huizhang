package com.bootdo.employment.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 用工需求工种实体类
 */
public class EmpDemandWorkTypeDO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long empDemandId;
    private String empDemandName;
    private Long workTypeId;
    private String workTypeName;
    private Double manHourFee;
    private Double commission;
    private Integer status;
    private Long userIdCreate;
    private Date gmtCreate;
    private Date gmtModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmpDemandId() {
        return empDemandId;
    }

    public void setEmpDemandId(Long empDemandId) {
        this.empDemandId = empDemandId;
    }

    public String getEmpDemandName() {
        return empDemandName;
    }

    public void setEmpDemandName(String empDemandName) {
        this.empDemandName = empDemandName;
    }

    public Long getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(Long workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public Double getManHourFee() {
        return manHourFee;
    }

    public void setManHourFee(Double manHourFee) {
        this.manHourFee = manHourFee;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getUserIdCreate() {
        return userIdCreate;
    }

    public void setUserIdCreate(Long userIdCreate) {
        this.userIdCreate = userIdCreate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "EmpDemandWorkTypeDO{" +
                "id=" + id +
                ", empDemandId=" + empDemandId +
                ", empDemandName='" + empDemandName + '\'' +
                ", workTypeId=" + workTypeId +
                ", workTypeName='" + workTypeName + '\'' +
                ", manHourFee=" + manHourFee +
                ", commission=" + commission +
                ", status=" + status +
                ", userIdCreate=" + userIdCreate +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}
