package com.bootdo.employment.domain;

import com.bootdo.system.domain.EnergyLogDO;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 工种实体类
 */
public class WorkTypeDO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userIdCreate;
    private String name;
    private Integer status;
    private Date gmtCreate;
    private Date gmtModified;
    private Double manHourFee;
    private Double commission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserIdCreate() {
        return userIdCreate;
    }

    public void setUserIdCreate(Long userIdCreate) {
        this.userIdCreate = userIdCreate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Double getManHourFee() {
        return manHourFee;
    }

    public void setManHourFee(Double manHourFee) {
        this.manHourFee = manHourFee;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return "WorkTypeDO{" +
                "id=" + id +
                ", userIdCreate=" + userIdCreate +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", manHourFee=" + manHourFee +
                ", commission=" + commission +
                '}';
    }
}
