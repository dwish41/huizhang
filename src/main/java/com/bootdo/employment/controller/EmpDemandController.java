package com.bootdo.employment.controller;

import com.bootdo.common.annotation.Log;
import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.employment.domain.EmpDemandDO;
import com.bootdo.employment.service.EmpDemandService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RequestMapping("/emp/empDemand")
@Controller
public class EmpDemandController extends BaseController {

	@Autowired
	EmpDemandService empDemandService;

	private String prefix="emp/empDemand"  ;

	@RequiresPermissions("emp:empDemand:empDemand")
	@GetMapping("")
	String empDemand(Model model) {
		return prefix + "/empDemand";
	}

	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<EmpDemandDO> list = empDemandService.list(query);
		int total = empDemandService.count(query);
		PageUtils pageUtil = new PageUtils(list, total);
		return pageUtil;
	}

	@RequiresPermissions("emp:empDemand:add")
	@Log("添加用工需求")
	@GetMapping("/add")
	String add(Model model) {
		return prefix + "/empDemand_add";
	}

	@RequiresPermissions("emp:empDemand:add")
	@Log("保存用工需求")
	@PostMapping("/save")
	@ResponseBody
	R save(EmpDemandDO empDemand) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (empDemandService.save(empDemand) > 0) {
			return R.ok();
		}
		return R.error();
	}

	@RequiresPermissions("emp:empDemand:edit")
	@Log("编辑用工需求")
	@GetMapping("/edit/{id}")
	String edit(Model model, @PathVariable("id") Long id) {
		EmpDemandDO empDemandDO = empDemandService.get(id);
		model.addAttribute("empDemand", empDemandDO);
		return prefix+"/edit";
	}

	@RequiresPermissions("emp:empDemand:edit")
	@Log("更新用工需求")
	@PostMapping("/update")
	@ResponseBody
	R update(EmpDemandDO empDemand) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (empDemandService.update(empDemand) > 0) {
			return R.ok();
		}
		return R.error();
	}

	@RequiresPermissions("emp:empDemand:remove")
	@Log("删除用工需求")
	@PostMapping("/remove")
	@ResponseBody
	R remove(Long id) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (empDemandService.remove(id) > 0) {
			return R.ok();
		}
		return R.error();
	}

	@RequiresPermissions("emp:empDemand:batchRemove")
	@Log("批量删除用工需求")
	@PostMapping("/batchRemove")
	@ResponseBody
	R batchRemove(@RequestParam("ids[]") Long[] ids) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		int r = empDemandService.batchremove(ids);
		if (r > 0) {
			return R.ok();
		}
		return R.error();
	}

	@PostMapping("/exit")
	@ResponseBody
	boolean exit(@RequestParam Map<String, Object> params) {
		return !empDemandService.exit(params);
	}
}
