package com.bootdo.employment.controller;

import com.bootdo.common.annotation.Log;
import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.employment.domain.WorkTypeDO;
import com.bootdo.employment.service.WorkTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/emp/workType")
@Controller
public class WorkTypeController extends BaseController {

	@Autowired
	WorkTypeService workTypeService;

	private String prefix="emp/workType"  ;

	@RequiresPermissions("emp:workType:workType")
	@GetMapping("")
	String workType(Model model) {
		return prefix + "/workType";
	}

	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<WorkTypeDO> workTypeList = workTypeService.list(query);
		int total = workTypeService.count(query);
		PageUtils pageUtil = new PageUtils(workTypeList, total);
		return pageUtil;
	}

	@GetMapping("/getList")
	@ResponseBody
	public List<WorkTypeDO> getList(@RequestParam Map<String, Object> params) {
		List<WorkTypeDO> workTypeList = workTypeService.list(params);
		return workTypeList;
	}

	@RequiresPermissions("emp:workType:add")
	@Log("添加工种")
	@GetMapping("/add")
	String add(Model model) {
		return prefix + "/add";
	}

	@RequiresPermissions("emp:workType:add")
	@Log("保存工种")
	@PostMapping("/save")
	@ResponseBody
	R save(WorkTypeDO workType) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (workTypeService.save(workType) > 0) {
			return R.ok();
		}
		return R.error();
	}

	@RequiresPermissions("emp:workType:edit")
	@Log("编辑工种")
	@GetMapping("/edit/{id}")
	String edit(Model model, @PathVariable("id") Long id) {
		WorkTypeDO workTypeDO = workTypeService.get(id);
		model.addAttribute("workType", workTypeDO);
		return prefix+"/edit";
	}

	@RequiresPermissions("emp:workType:edit")
	@Log("更新工种")
	@PostMapping("/update")
	@ResponseBody
	R update(WorkTypeDO workType) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (workTypeService.update(workType) > 0) {
			return R.ok();
		}
		return R.error();
	}

	/*@RequiresPermissions("emp:workType:edit")
	@Log("更新工种")
	@PostMapping("/updatePeronal")
	@ResponseBody
	R updatePeronal(WorkTypeDO workType) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (workTypeService.updatePersonal(workType) > 0) {
			return R.ok();
		}
		return R.error();
	}*/


	@RequiresPermissions("emp:workType:remove")
	@Log("删除工种")
	@PostMapping("/remove")
	@ResponseBody
	R remove(Long id) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (workTypeService.remove(id) > 0) {
			return R.ok();
		}
		return R.error();
	}

	@RequiresPermissions("emp:workType:batchRemove")
	@Log("批量删除工种")
	@PostMapping("/batchRemove")
	@ResponseBody
	R batchRemove(@RequestParam("ids[]") Long[] ids) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		int r = workTypeService.batchremove(ids);
		if (r > 0) {
			return R.ok();
		}
		return R.error();
	}

	@PostMapping("/exit")
	@ResponseBody
	boolean exit(@RequestParam Map<String, Object> params) {
		return !workTypeService.exit(params);
	}
}
