package com.bootdo.common.shiro;

import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

import java.io.Serializable;

/**
 * 手机号登陆token
 *
 * @author zhouxm
 */
public class MobileToken implements HostAuthenticationToken, RememberMeAuthenticationToken, Serializable {
    private String mobile;
    private boolean rememberMe;
    private String host;

    public MobileToken() {
        this.rememberMe = false;
    }

    public MobileToken(String mobile) {
        this(mobile, false, null);
    }

    public MobileToken(String mobile, boolean rememberMe) {
        this(mobile, rememberMe, null);
    }

    public MobileToken(String mobile, boolean rememberMe, String host) {
        this.mobile = mobile;
        this.rememberMe = rememberMe;
        this.host = host;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public boolean isRememberMe() {
        return false;
    }

    @Override
    public Object getPrincipal() {
        return mobile;
    }

    @Override
    public Object getCredentials() {
        return mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
