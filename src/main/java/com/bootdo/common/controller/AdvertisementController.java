package com.bootdo.common.controller;

import com.bootdo.common.domain.AdvertisementDO;
import com.bootdo.common.service.AdvertisementService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-15 16:47:04
 */
 
@Controller
@RequestMapping("/system/advertisement")
public class AdvertisementController extends BaseController {
	@Autowired
	private AdvertisementService advertisementService;
	
	@GetMapping()
	@RequiresPermissions("system:advertisement:advertisement")
	String Advertisement(){
	    return "common/advertisement/advertisement_list";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:advertisement:advertisement")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<AdvertisementDO> advertisementList = advertisementService.list(query);
		int total = advertisementService.count(query);
		PageUtils pageUtils = new PageUtils(advertisementList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:advertisement:add")
	String add(Model model){
		model.addAttribute("attachId", UUID.randomUUID());
	    return "common/advertisement/advertisement_add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:advertisement:edit")
	String edit(@PathVariable("id") Long id,Model model){
		AdvertisementDO advertisement = advertisementService.get(id);
		model.addAttribute("advertisement", advertisement);
		model.addAttribute("attachId", UUID.randomUUID());
	    return "common/advertisement/advertisement_edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:advertisement:add")
	public R save( AdvertisementDO advertisement){
		if(advertisementService.save(advertisement)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:advertisement:edit")
	public R update( AdvertisementDO advertisement){
		advertisementService.update(advertisement);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:advertisement:remove")
	public R remove( Long id){
		if(advertisementService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:advertisement:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		advertisementService.batchRemove(ids);
		return R.ok();
	}


	@ResponseBody
	@PostMapping("/advertisementList")
	public R advertisementList(){
		List<AdvertisementDO> advertisementList = advertisementService.list(null);
		return R.ok().put("advertisementList",advertisementList);
	}



}
