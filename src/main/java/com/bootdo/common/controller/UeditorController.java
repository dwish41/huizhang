package com.bootdo.common.controller;

import com.baidu.ueditor.ActionEnter;
import com.bootdo.common.config.BootdoConfig;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
@RequestMapping("/common/js/ueditor/jsp")
public class UeditorController {

    @Resource
    private BootdoConfig bootdoConfig;

    /**
     * 设置测试页
     *
     * @return
     */
    @RequestMapping("/index")
    public String index() {
        return "system/ueditor";
    }

    /**
     * 读取配置的请求
     * @param request
     * @param response
     */
    @RequestMapping("/controller")
    @ResponseBody
    public void getConfigInfo(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/json");
        //获取classPath路径
        String rootPath = ClassUtils.getDefaultClassLoader().getResource("").getPath() + "static";
        //将存盘目录设置到request 作用域 在百度上传的类中读取
        request.setAttribute("uploadPath",bootdoConfig.getUploadPath());

        System.out.println("rootPath:" + rootPath);
        try {
            String exec = new ActionEnter(request, rootPath).exec();
            System.out.println("exec:" + exec);
            PrintWriter writer = response.getWriter();
            writer.write(exec);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
