package com.bootdo.common.service;

import com.bootdo.common.domain.FileDO;
import com.bootdo.common.utils.R;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 文件上传
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-19 16:02:20
 */
public interface FileService {

	FileDO get(String id);

	List<FileDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);

	int save(FileDO sysFile);

	int update(FileDO sysFile);

	int remove(Long id);

	int batchRemove(Long[] ids);

	/**
	 * 判断一个文件是否存在
	 * @param url FileDO中存的路径
	 * @return
	 */
    Boolean isExist(String url);

	/**
	 * 图片文件上传
	 * @param file
	 * @param fileDO
	 * @return
	 */
    R upload(MultipartFile[] file, FileDO fileDO);

	/**
	 * 获取文件列表数据
	 * @param attachId
	 * @return
	 */
	List<FileDO> queryListByAttachId(String attachId);
}
