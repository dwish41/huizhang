package com.bootdo.common.service;

import com.bootdo.common.domain.Area;
import com.bootdo.system.domain.DeptDO;

import java.util.List;
import java.util.Map;

/***
 *
 * @Function 地区接口类
 * @Author xujiajia
 * @Date 2019/9/11 17:18
 * @Param
 * @Return
 ***/
public interface AreaService {

    List<Area> queryAreaInfo(String areaId,String flag);

    String getAreaAllName(String areaId);

    List<Area> list(Map<String, Object> query);

    Area get(Long pId);

    int insert(Area area);

    int update(Area area);
}
