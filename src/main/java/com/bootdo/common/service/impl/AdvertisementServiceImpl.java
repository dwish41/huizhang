package com.bootdo.common.service.impl;

import com.bootdo.common.dao.AdvertisementDao;
import com.bootdo.common.domain.AdvertisementDO;
import com.bootdo.common.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {
	@Autowired
	private AdvertisementDao advertisementDao;
	
	@Override
	public AdvertisementDO get(Long id){
		return advertisementDao.get(id);
	}
	
	@Override
	public List<AdvertisementDO> list(Map<String, Object> map){
		return advertisementDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return advertisementDao.count(map);
	}
	
	@Override
	public int save(AdvertisementDO advertisement){
		return advertisementDao.save(advertisement);
	}
	
	@Override
	public int update(AdvertisementDO advertisement){
		return advertisementDao.update(advertisement);
	}
	
	@Override
	public int remove(Long id){
		return advertisementDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return advertisementDao.batchRemove(ids);
	}
	
}
