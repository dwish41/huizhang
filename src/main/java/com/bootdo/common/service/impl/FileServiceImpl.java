package com.bootdo.common.service.impl;

import com.bootdo.common.config.BootdoConfig;
import com.bootdo.common.utils.R;
import com.bootdo.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.*;

import com.bootdo.common.dao.FileDao;
import com.bootdo.common.domain.FileDO;
import com.bootdo.common.service.FileService;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
public class FileServiceImpl implements FileService {
	@Autowired
	private FileDao sysFileMapper;

	@Autowired
	private BootdoConfig bootdoConfig;
	@Override
	public FileDO get(String id){
		return sysFileMapper.get(id);
	}

	@Override
	public List<FileDO> list(Map<String, Object> map){
		return sysFileMapper.list(map);
	}

	@Override
	public int count(Map<String, Object> map){
		return sysFileMapper.count(map);
	}

	@Override
	public int save(FileDO sysFile){
		return sysFileMapper.save(sysFile);
	}

	@Override
	public int update(FileDO sysFile){
		return sysFileMapper.update(sysFile);
	}

	@Override
	public int remove(Long id){
		return sysFileMapper.remove(id);
	}

	@Override
	public int batchRemove(Long[] ids){
		return sysFileMapper.batchRemove(ids);
	}

    @Override
    public Boolean isExist(String url) {
		Boolean isExist = false;
		if (!StringUtils.isEmpty(url)) {
			String filePath = url.replace("/files/", "");
			filePath = bootdoConfig.getUploadPath() + filePath;
			File file = new File(filePath);
			if (file.exists()) {
				isExist = true;
			}
		}
		return isExist;
	}

	/**
	 * 图片文件上传
	 * @param files
	 * @param fileDO
	 * @return
	 */
	@Override
	public R upload(MultipartFile[] files, FileDO fileDO) {
		String rootPath = bootdoConfig.getUploadPath();
		//  /images/upload/
		String uploadPath = "/images/upload/" + fileDO.getAttachId();
		if(files != null && files.length > 0){
			//循环获取file数组中得文件
			for(int i = 0;i < files.length;i++){
				MultipartFile file = files[i];
				//保存文件
				if (!file.isEmpty()){
					// 生成文件上传路径，先对该路径是否存在进行判断
					String folder = rootPath + uploadPath;
					File floder = new File(folder);
					if (!floder.exists()) {
						try {
							floder.mkdirs();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					try {
						// 将获取到的附件file,transferTo写入到指定的位置(即:创建dest时，指定的路径)
						String fileName = file.getOriginalFilename();
						String filePath = uploadPath + "/" +fileName;
						file.transferTo(new File(rootPath + filePath));

						//保存到数据库
						fileDO.setId(UUID.randomUUID().toString());
						fileDO.setCreateDate(new Date());
						fileDO.setFileUrl("/images/upload/" + fileDO.getAttachId() + "/" + fileName);
						fileDO.setFileName(fileName);
						//有效
						fileDO.setDelFlag("0");
						sysFileMapper.save(fileDO);
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}

	@Override
	public List<FileDO> queryListByAttachId(String attachId) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("attachId",attachId);
		return sysFileMapper.list(map);
	}
}
