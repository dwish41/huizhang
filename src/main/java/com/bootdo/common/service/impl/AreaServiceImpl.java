package com.bootdo.common.service.impl;

import com.bootdo.common.dao.AreaDao;
import com.bootdo.common.domain.Area;
import com.bootdo.common.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName AreaServiceImpl
 * @Description 地区接口实现类
 * @Author xujiajia
 * @Date2019/9/11 17:18
 * @Version V1.0
 **/
@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaDao areaDao;

    /***
     *
     * @Function 获取地区关系
     * @Author xujiajia
     * @Date 2019/9/11 17:20
     * @Param [areaId, areaIdLength]
     * @Return java.util.List<com.bootdo.common.domain.Area>
     ***/
    @Override
    public List<Area> queryAreaInfo(String areaId,String flag) {
        List<Area> list = new ArrayList<Area>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("areaId", areaId);
        paramMap.put("flag", flag);
        list = areaDao.selectAreaInfoByLevel(paramMap);
        return list;
    }

    /***
     *
     * @Function 获取地区全名
     * @Author xujiajia
     * @Date 2019/9/12 9:35
     * @Param [areaId]
     * @Return java.lang.String
     ***/
    @Override
    public String getAreaAllName(String areaId) {
        StringBuffer areaAllName = new StringBuffer();
        int length = areaId.length();
        if (length >= 6) {
            areaAllName.append(getAreaNameById(areaId.substring(0, 6)));
            if (length >= 10) {
                areaAllName.append(getAreaNameById(areaId.substring(0, 10)));
                if (length >= 15) {
                    areaAllName.append(getAreaNameById(areaId.substring(0, 15)));
                    if (length >= 20) {
                        areaAllName.append(getAreaNameById(areaId.substring(0, 20)));
                    }
                }
            }
        }
        return areaAllName.toString();
    }

    /***
     *
     * @Function 根据ID获取地区名
     * @Author xujiajia
     * @Date 2019/9/12 9:39
     * @Param [areaId]
     * @Return java.lang.String
     ***/
    public String getAreaNameById(String areaId) {
        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("areaId", areaId);
        return areaDao.getAreaNameById(paramMap);
    }

    @Override
    public List<Area> list(Map<String, Object> map) {
        return areaDao.list(map);
    }

    @Override
    public Area get(Long pId) {
        return areaDao.get(pId);
    }

    @Override
    public int insert(Area area) {
        return areaDao.insert(area);
    }

    @Override
    public int update(Area area) {
        return areaDao.update(area);
    }
}
