package com.bootdo.common.config;

public class Constant {
    public final static int ERROR_CODE = 500;
    //演示系统账户
    public static String DEMO_ACCOUNT = "test";
    //自动去除表前缀
    public static String AUTO_REOMVE_PRE = "true";
    //停止计划任务
    public static String STATUS_RUNNING_STOP = "stop";
    //开启计划任务
    public static String STATUS_RUNNING_START = "start";
    //通知公告阅读状态-未读
    public static String OA_NOTIFY_READ_NO = "0";
    //通知公告阅读状态-已读
    public static int OA_NOTIFY_READ_YES = 1;
    //部门根节点id
    public static Long DEPT_ROOT_ID = 0L;
    //缓存方式
    public static String CACHE_TYPE_REDIS ="redis";

    public static String LOG_ERROR = "error";
    /**
     * 短信验证码数据字典key
     */
    public final static String VERIFY_CODE_KEY = "verify_code";
    /**
     * 首次登陆能量值数据字典key
     */
    public final static String REGISTER_ENERGY_KEY = "register_energy";
    /**
     * 首次登陆能量值默认值
     */
    public final static int REGISTER_ENERGY_VALUE = 20;
    /**
     * 短信验证码有效时间
     */
    public final static Long REDIS_CODE_TIMEOUT = 5L;
    /**
     * 短信验证码是否使用，1使用
     */
    public final static String CODE_USED = "1";
    /**
     * 收藏点赞
     */
    public final static String COLLECTION_THUMB_ENABLE = "1";
    /**
     * 取消收藏点赞
     */
    public final static String COLLECTION_THUMB_DISABLE = "0";

    //管理员角色
    public static String ADMIN_ROLE = "1";

    //人力资源角色
    public static String RL_ROLE = "2";

    //代理角色
    public static String AGENT_ROLE = "3";

    //员工角色
    public static String STAFF_ROLE = "4";

    //默认密码
    public static String PASSWORD = "111111";

}
