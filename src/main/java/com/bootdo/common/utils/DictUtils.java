package com.bootdo.common.utils;

import com.bootdo.common.config.ApplicationContextRegister;
import com.bootdo.common.dao.DictDao;
import com.bootdo.common.domain.DictDO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取数据字典工具类
 *
 * @author zhouxm
 */
public class DictUtils {
    /**
     * 根据类型获取唯一字典项
     * @param type 类型
     * @return
     */
    public static DictDO getDictByType(String type) {
        List<DictDO> dictDOList = getDictListByType(type);
        if (dictDOList != null) {
            return dictDOList.get(0);
        }
        return null;
    }

    /**
     * 根据类型获取字典项
     * @param type 类型
     * @return
     */
    public static List<DictDO> getDictListByType(String type) {
        DictDao dictDao = ApplicationContextRegister.getBean(DictDao.class);
        Map<String, Object> param = new HashMap<>(16);
        param.put("type", type);
        return dictDao.list(param);
    }
}
