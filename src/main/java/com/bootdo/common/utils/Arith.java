package com.bootdo.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Arith {

	// 默认除法运算精度 

	private static final int DEF_DIV_SCALE = 10;

	// 这个类不能实例化 

	private Arith() {

	}

	/** 

	 * 提供精确的加法运算。 

	 * @param v1 被加数 

	 * @param v2 加数 

	 * @return 两个参数的和 

	 */

	public static double add(double v1, double v2) {

		BigDecimal b1 = new BigDecimal(Double.toString(v1));

		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.add(b2).doubleValue();

	}

	/** 

	 * 提供精确的减法运算。 

	 * @param v1 被减数 

	 * @param v2 减数 

	 * @return 两个参数的差 

	 */

	public static double sub(double v1, double v2) {

		BigDecimal b1 = new BigDecimal(Double.toString(v1));

		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.subtract(b2).doubleValue();

	}

	/** 

	 * 提供精确的乘法运算。 

	 * @param v1 被乘数 

	 * @param v2 乘数 

	 * @return 两个参数的积 

	 */

	public static double mul(double v1, double v2) {

		BigDecimal b1 = new BigDecimal(Double.toString(v1));

		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.multiply(b2).doubleValue();

	}
	
	/**
	 * 
	 * @Function:提供精确的n次方运算 
	 * @Author:xujiajia
	 * @date: 2018-6-27 下午3:36:49 
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static double pow(double v1, double v2) {
		return Math.pow(v1,v2);
	}

	/** 

	 * 提供（相对）精确的除法运算，当发生除不尽的情况时，精确到 

	 * 小数点以后 10 位，以后的数字四舍五入。 

	 * @param v1 被除数 

	 * @param v2 除数 

	 * @return 两个参数的商 

	 */

	public static double div(double v1, double v2) {

		return div(v1, v2, DEF_DIV_SCALE);

	}

	/** 

	 * 提供（相对）精确的除法运算。当发生除不尽的情况时，由 scale 参数指 

	 * 定精度，以后的数字四舍五入。 

	 * @param v1 被除数 

	 * @param v2 除数 

	 * @param scale 表示表示需要精确到小数点以后几位。 

	 * @return 两个参数的商 

	 */

	public static double div(double v1, double v2, int scale) {
		if (v2 == 0) {
			return 0;
		}

		if (scale < 0) {

			throw new IllegalArgumentException(

			"The scale must be a positive integer or zero");

		}

		BigDecimal b1 = new BigDecimal(Double.toString(v1));

		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();

	}

	/** 

	 * 提供精确的小数位四舍五入处理。 

	 * @param v 需要四舍五入的数字 

	 * @param scale 小数点后保留几位 

	 * @return 四舍五入后的结果 

	 */

	public static double round(double v, int scale) {

		if (scale < 0) {

			throw new IllegalArgumentException(

			"The scale must be a positive integer or zero");

		}

		BigDecimal b = new BigDecimal(Double.toString(v));

		BigDecimal one = new BigDecimal("1");

		return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();

	}
	/**
	 * 
	 * 功能：前台显示保留两位价格小数,四舍五入
	 * @Title: round 
	 * @Date: 2010-6-24下午03:24:55
	 * @author wufei wufei@hpe.com 
	 * @param v
	 * @return
	 */
	public static double round(String v) {
		if("".equals(v) || null==v) {
			return 0.00;
		}
		
		int scale = 2;

		BigDecimal b = new BigDecimal(v);

		BigDecimal one = new BigDecimal("1");

		return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();

	}
	/**
	 *
	 * 功能：整數or小數 求餘  
	 * @Title: remainder  
	 * @Date: Jun 24, 201010:24:46 AM 
	 * @author zhengshanjian@hpe.com  
	 * @param a
	 * @param b
	 * @return
	 */
	public static String remainder(String a,String b) {
		
		BigDecimal amount = new BigDecimal(a);

		BigDecimal year_fee = new BigDecimal(b);
		
		String x = amount.remainder(year_fee).toString();
		
		
		return x;

	}
	
	/****
	 * 
	 * @Function:double保留小数
	 * @Author:xujiajia
	 * @date: 2019-2-13 上午10:37:14 
	 * @param value
	 * @param scale
	 * @return
	 */
	public static String format1(double value,int scale) {
		DecimalFormat df = new DecimalFormat();
		if(scale == 2) {
			df = new DecimalFormat("#0.00");//保留两位小数
		}else if(scale == 3) {
			df = new DecimalFormat("#0.000");//保留三位小数
		}
		String st=df.format(value);
		return st;
	}
	
};
