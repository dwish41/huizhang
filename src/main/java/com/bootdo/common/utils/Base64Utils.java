package com.bootdo.common.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author zhouxm
 */
public class Base64Utils {
    private final static Base64.Encoder ENCODER = Base64.getEncoder();
    private final static Base64.Decoder DECODER = Base64.getDecoder();

    /**
     * 给字符串加密
     * @param text
     * @return
     */
    public static String encode(String text) {
        byte[] textByte = text.getBytes(StandardCharsets.UTF_8);
        return ENCODER.encodeToString(textByte);
    }

    /**
     * 将加密后的字符串进行解密
     * @param encodedText
     * @return
     */
    public static String decode(String encodedText) {
        return new String(DECODER.decode(encodedText), StandardCharsets.UTF_8);
    }

    public static void main(String[] args) {

        String username = "Miracle Luna";
        String password = "111111";

        // 加密
        System.out.println("====  [加密后] 用户名/密码  =====");
        System.out.println(Base64Utils.encode(username));
        System.out.println(Base64Utils.encode(password));

        // 解密
        System.out.println("\n====  [解密后] 用户名/密码  =====");
        System.out.println(Base64Utils.decode(Base64Utils.encode(username)));
        System.out.println(Base64Utils.decode(Base64Utils.encode(password)));
    }
	
}
