package com.bootdo.common.utils;

import java.util.HashMap;
import java.util.Map;

public class R extends HashMap<String, Object> {
	public static String CODE_KEY = "code";
	public static String MSG_KEY = "msg";
	public static String DATA_KEY = "data";
	private static final long serialVersionUID = 1L;

	public R() {
		put(CODE_KEY, 0);
		put(MSG_KEY, "操作成功");
	}

	public static R error() {
		return error(1, "操作失败");
	}

	public static R error(String msg) {
		return error(500, msg);
	}

	public static R error(int code, String msg) {
		R r = new R();
		r.put(CODE_KEY, code);
		r.put(MSG_KEY, msg);
		return r;
	}

	public static R ok(String msg) {
		R r = new R();
		r.put(MSG_KEY, msg);
		return r;
	}

	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}

	public static R ok(Object data) {
		R r = new R();
		r.put(CODE_KEY, 0);
		r.put(DATA_KEY,data);
		return r;
	}

	public static R ok() {
		return new R();
	}

	@Override
	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
