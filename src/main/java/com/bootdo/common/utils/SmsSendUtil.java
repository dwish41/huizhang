package com.bootdo.common.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 短信发送接口
 */
public class SmsSendUtil {

    public static final String appid = "N23GEBBO";

    public static final String tid = "12366";

    public static final String time = "5";

    public static final String url = "https://pretest.xfpaas.com/dripsms/sms";

    public static R sendSms(String phone, String code) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("appid",appid);
        jsonObject.put("tid",tid);
        jsonObject.put("phone",phone);
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("time",time);
        jsonObject1.put("code",code);
        jsonObject.put("tp",jsonObject1);
        String message = HttpRequestUtil.postJSON(url,jsonObject);
        if(null != message) {
            //转换成为JSONObject对象
            JSONObject jsonObj = JSONObject.parseObject(message);
            if(StringUtils.equals("000000", (String) jsonObj.get("code"))) {
                return R.ok();
            }else {
                return R.error((String) jsonObj.get("desc"));
            }
        }
        System.out.println(message);
        return R.error("短信发送失败");
    }

    public static void main(String[] args) {
        try {
            sendSms("13855181552","6666");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
