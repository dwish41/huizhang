package com.bootdo.common.utils;

/**
 * 数字工具类
 *
 * @author zhouxm
 */
public class MathUtils {
    /**
     * 获取4位随机验证码
     *
     * @return 返回4位随机数
     */
    public static String getVerifyCode() {
        return String.valueOf((int) ((Math.random() * 9 + 1) * 1000));
    }

}
