package com.bootdo.common.pojo;

import lombok.Data;

/**
 * 用户名密码登陆实体参数
 *
 * @author zhouxm
 */
@Data
public class UserReq {
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 图形验证码
     */
    private String verify;
    /**
     * 手机验证码
     */
    private String code;
    /**
     * 手机号
     */
    private String mobile;
}
