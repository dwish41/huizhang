package com.bootdo.common.dao;

import com.bootdo.common.domain.AdvertisementDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-15 16:47:04
 */
@Mapper
public interface AdvertisementDao {

	AdvertisementDO get(Long id);
	
	List<AdvertisementDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(AdvertisementDO advertisement);
	
	int update(AdvertisementDO advertisement);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
