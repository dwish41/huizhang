package com.bootdo.common.dao;

import com.bootdo.common.domain.Area;

import java.util.List;
import java.util.Map;

public interface AreaDao {

    int insertSelective(Area record);

    List<Area> selectAreaInfoByLevel(Map<String, Object> paramMap);

    String getAreaNameById(Map<String, Object> paramMap);

    List<Area> list(Map<String, Object> map);

    Area get(Long pId);

    int insert(Area area);

    int update(Area area);
}
