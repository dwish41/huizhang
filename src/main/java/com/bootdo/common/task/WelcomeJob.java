package com.bootdo.common.task;

import com.bootdo.oa.domain.Response;
import com.bootdo.system.service.SessionService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class WelcomeJob implements Job {

	@Autowired
	SimpMessagingTemplate template;

    @Autowired
    private SessionService sessionService;

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        if(sessionService.listOnlineUser().size() > 0) {
            template.convertAndSendToUser(sessionService.listOnlineUser().get(0).toString(), "/queue/notifications", "发给在线用户一条消息");
        }
    	template.convertAndSend("/topic/getResponse", new Response("欢迎体验后台登录系统,这是一个任务计划，使用了websocket和quzrtz技术，可以在计划列表中取消!" ));

    }

}
