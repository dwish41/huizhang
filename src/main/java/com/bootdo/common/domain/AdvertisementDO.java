package com.bootdo.common.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2020-10-15 16:47:04
 */
public class AdvertisementDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	//轮播名称
	private String advName;
	//轮播名称
	private String advCode;
	//广告说明
	private String advContent;
	//图片id
	private String attachId;
	//0.正常 1.删除
	private String delFlag;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdvName() {
		return advName;
	}

	public void setAdvName(String advName) {
		this.advName = advName;
	}

	public String getAdvCode() {
		return advCode;
	}

	public void setAdvCode(String advCode) {
		this.advCode = advCode;
	}

	public String getAdvContent() {
		return advContent;
	}

	public void setAdvContent(String advContent) {
		this.advContent = advContent;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
}
