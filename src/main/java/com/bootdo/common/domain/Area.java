package com.bootdo.common.domain;

import java.util.Date;

public class Area {

    private Long areaId;

    private String areaName;

    private String operUser;

    private Date operTime;

    private Long removeTag;

    private Long parentId;

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getOperUser() {
        return operUser;
    }

    public void setOperUser(String operUser) {
        this.operUser = operUser;
    }

    public Date getOperTime() {
        return operTime;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    public Long getRemoveTag() {
        return removeTag;
    }

    public void setRemoveTag(Long removeTag) {
        this.removeTag = removeTag;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
