package com.bootdo.front.controller;

import com.bootdo.common.annotation.Log;
import com.bootdo.common.utils.R;
import com.bootdo.common.utils.StringUtils;
import com.bootdo.system.domain.EnergyCouponDO;
import com.bootdo.system.domain.EnergyLogDO;
import com.bootdo.system.domain.UserDO;
import com.bootdo.system.service.EnergyCouponService;
import com.bootdo.system.service.EnergyLogService;
import com.bootdo.system.service.LoginService;
import com.bootdo.system.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/front")
@Slf4j
public class FrontEnergyCouponController {

    @Autowired
    private UserService userService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private EnergyLogService energyLogService;

    @Autowired
    private EnergyCouponService energyCouponService;

    @Log("体验场景积分能量增加")
    @PostMapping("/addEnergy")
    @ResponseBody
    public R addEnergy(UserDO userDO) {
        //判断某一个场景是否已经新增能量值
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userId",userDO.getUserId());
        map.put("energyType",userDO.getEnergyType());
        EnergyLogDO energyLogDO = energyLogService.getEnergyLogDetail(map);
        if(energyLogDO == null) {
            //表示第一次增加
            energyLogDO = new EnergyLogDO();
            energyLogDO.setUserId(userDO.getUserId());
            String energyDescription = getEnergyDescription(userDO.getEnergyType());
            energyLogDO.setEnergyDescription(energyDescription);
            energyLogDO.setcreateDate(new Date());
            energyLogService.save(energyLogDO);

            //用户能量值增加
            userDO.setEnergy(20);
            loginService.addEnergy(userDO);
        }
        return R.ok();
    }

    /**
     * 类型说明
     * @param energyType
     * @return
     */
    private String getEnergyDescription(String energyType) {
        String energyDescription = "";
        if(StringUtils.equals("1",energyType)) {
            energyDescription = "体验讯飞智慧医疗增加20个积分";
        }else if(StringUtils.equals("2",energyType)) {
            energyDescription = "体验讯飞智慧城市增加20个积分";
        }else if(StringUtils.equals("3",energyType)) {
            energyDescription = "体验讯飞智慧生活增加20个积分";
        }else if(StringUtils.equals("4",energyType)) {
            energyDescription = "体验讯飞智慧教育增加20个积分";
        }else if(StringUtils.equals("5",energyType)) {
            energyDescription = "体验讯飞智慧司法增加20个积分";
        }else if(StringUtils.equals("6",energyType)) {
            energyDescription = "体验讯飞智能汽车疗增加20个积分";
        }else if(StringUtils.equals("7",energyType)) {
            energyDescription = "体验讯飞智能硬件增加20个积分";
        }else if(StringUtils.equals("8",energyType)) {
            energyDescription = "体验讯飞大厦增加20个积分";
        }
        return energyDescription;
    }

    /**
     * 判断当前优惠券是否可以兑换
     * @param energyCouponDO
     * @return
     */
    @PostMapping("/judeExchangeCoupon")
    @ResponseBody
    public R judeExchangeCoupon(EnergyCouponDO energyCouponDO) {
        //获取用户能量积分值
        UserDO userDO = userService.get(energyCouponDO.getUserId());
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userId",userDO.getUserId());
        //30积分，20元无门槛讯飞商城优惠券 couponType=1
        //70积分，满500元减50元讯飞商城优惠券 couponType=2
        //170积分，满5000元减200元讯飞商城优惠券 couponType=3
        if(userDO.getEnergy() < 30) {
            return R.error("当前能量积分不足");
        }else if(userDO.getEnergy() >= 30 && userDO.getEnergy() < 70) {
            if(!StringUtils.equals("1",energyCouponDO.getCouponType())) {
                return R.error("当前能量积分不足");
            }else {
                //判断优惠券是否已经兑换
                map.put("couponType",energyCouponDO.getCouponType());
                EnergyCouponDO energyCouponDO1 = energyCouponService.getEnergyCouponDetail(map);
                if(null != energyCouponDO1) {
                    return R.error("当前类型优惠券已经兑换，不可重复兑换");
                }
            }
        }else if(userDO.getEnergy() >= 70 && userDO.getEnergy() < 170) {
            if(!StringUtils.equals("3",energyCouponDO.getCouponType())) {
                return R.error("当前能量积分不足");
            }else {
                //判断优惠券是否已经兑换
                map.put("couponType",energyCouponDO.getCouponType());
                EnergyCouponDO energyCouponDO1 = energyCouponService.getEnergyCouponDetail(map);
                if(null != energyCouponDO1) {
                    return R.error("当前类型优惠券已经兑换，不可重复兑换");
                }
            }
        }else if(userDO.getEnergy() >= 170) {
            //判断优惠券是否已经兑换
            map.put("couponType",energyCouponDO.getCouponType());
            EnergyCouponDO energyCouponDO1 = energyCouponService.getEnergyCouponDetail(map);
            if(null != energyCouponDO1) {
                return R.error("当前类型优惠券已经兑换，不可重复兑换");
            }
        }
        return R.ok();
    }

    /**
     * 优惠券兑换
     * @param energyCouponDO
     * @return
     */
    @PostMapping("/exchangeCoupon")
    @ResponseBody
    public R exchangeCoupon(EnergyCouponDO energyCouponDO) {
        energyCouponDO.setCreateDate(new Date());
        energyCouponDO.setUsedType("2");//1.已使用 2.未使用
        energyCouponDO.setExchangeType("1");//1.已兑换 2.未兑换
        String couponDescription = getCouponDescrption(energyCouponDO.getCouponType());
        energyCouponDO.setCouponDescrption(couponDescription);
        energyCouponService.save(energyCouponDO);
        return R.ok();
    }

    private String getCouponDescrption(String couponType) {
        String energyDescription = "";
        if(StringUtils.equals("1",couponType)) {
            energyDescription = "20元无门槛讯飞商城优惠券";
        }else if(StringUtils.equals("2",couponType)) {
            energyDescription = "满500元减50元讯飞商城优惠券";
        }else if(StringUtils.equals("3",couponType)) {
            energyDescription = "满5000元减200元讯飞商城优惠券";
        }
        return energyDescription;
    }
}
