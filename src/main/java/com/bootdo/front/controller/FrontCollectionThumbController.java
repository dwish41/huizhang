package com.bootdo.front.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.R;
import com.bootdo.system.domain.CollectionDO;
import com.bootdo.system.domain.ThumbDO;
import com.bootdo.system.service.CollectionThumbService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 前台收藏与点赞控制器
 *
 * @author zhouxm
 */
@Controller
@RequestMapping("/front")
@Slf4j
public class FrontCollectionThumbController extends BaseController {
    @Autowired
    private CollectionThumbService collectionThumbService;

    @PostMapping("/saveCollection")
    @ResponseBody
    public R saveCollection(CollectionDO collectionDO) {
        try {
            collectionThumbService.saveCollection(collectionDO);
        } catch (Exception e) {
            log.error("收藏失败：", e);
            return R.error();
        }
        return R.ok();
    }

    @PostMapping("/updateCollection")
    @ResponseBody
    public R updateCollection(CollectionDO collectionDO) {
        try {
            collectionThumbService.updateCollection(collectionDO);
        } catch (Exception e) {
            log.error("修改失败：", e);
            return R.error();
        }
        return R.ok();
    }

    @PostMapping("/saveThumb")
    @ResponseBody
    public R saveThumb(ThumbDO thumbDO) {
        try {
            collectionThumbService.saveThumb(thumbDO);
        } catch (Exception e) {
            log.error("点赞失败：", e);
            return R.error();
        }
        return R.ok();
    }

    @PostMapping("/updateThumb")
    @ResponseBody
    public R updateThumb(ThumbDO thumbDO) {
        try {
            collectionThumbService.updateThumb(thumbDO);
        } catch (Exception e) {
            log.error("修改失败：", e);
            return R.error();
        }
        return R.ok();
    }
    @GetMapping("/groupCollectionByProduct")
    @ResponseBody
    public R groupCollectionByProduct() {
        try {
            List<Map<String, Object>> map = collectionThumbService.groupCollectionByProduct();
            return R.ok().put("data", map);
        } catch (Exception e) {
            log.error("统计收藏失败：", e);
            return R.error();
        }
    }

    @GetMapping("/groupThumbByProduct")
    @ResponseBody
    public R groupThumbByProduct() {
        try {
            List<Map<String, Object>> map = collectionThumbService.groupThumbByProduct();
            return R.ok().put("data", map);
        } catch (Exception e) {
            log.error("统计点赞失败：", e);
            return R.error();
        }
    }
}
