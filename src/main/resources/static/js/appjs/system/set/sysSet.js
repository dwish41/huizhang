var prefix = "/sys/sysSet"

$(function(){
    load();
});

function reLoad() {
    load();
}

function load() {
    $.ajax({
        url : prefix + "/list",
        type : "post",
        success : function(r) {
            if (r.code == 0) {
                var setDOList=r.setDOList
                for (var i=0;i<setDOList.length;i++){
                    $("#"+setDOList[i].setCode).val(setDOList[i].setValue)
                }
            }
        }
    });
}

function updateCompany() {
    var companyName=$("#companyName").val()
    var copyright=$("#copyright").val()
    var recordNo=$("#recordNo").val()
    var technicalSupport=$("#technicalSupport").val()
    $.ajax({
        url : prefix + "/updateCompany",
        type : "post",
        data : {
            'companyName' : companyName,
            'copyright': copyright,
            'recordNo': recordNo,
            'technicalSupport': technicalSupport
        },
        success : function(r) {
            if (r.code == 0) {
                layer.msg(r.msg);
                reLoad();
            } else {
                layer.msg(r.msg);
            }
        }
    });
}

function updatePhone() {
    var consultPhone=$("#consultPhone").val()
    var complainPhone=$("#complainPhone").val()
    var faxPhone=$("#faxPhone").val()
    $.ajax({
        url : prefix + "/updatePhone",
        type : "post",
        data : {
            'consultPhone' : consultPhone,
            'complainPhone': complainPhone,
            'faxPhone': faxPhone
        },
        success : function(r) {
            if (r.code == 0) {
                layer.msg(r.msg);
                reLoad();
            } else {
                layer.msg(r.msg);
            }
        }
    });
}

function updateWeChatNum() {
    var weChatNum=$("#weChatNum").val()
    $.ajax({
        url : prefix + "/updateWeChatNum",
        type : "post",
        data : {
            'weChatNum' : weChatNum
        },
        success : function(r) {
            if (r.code == 0) {
                layer.msg(r.msg);
                reLoad();
            } else {
                layer.msg(r.msg);
            }
        }
    });
}



