$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/system/company/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			companyName : {
				required : true,
				maxlength : 50
			},
			companyCode : {
				required : true,
				isCompanyCode : false
			},
			applyName : {
				required : true,
				maxlength : 50
			},
			applyCardNumber: {
				required:true,
				isIdentity: true
			},
			applyMobile:{
				required:true,
				isPhone:true
			},
		},
		messages : {
			companyName : {
				required : icon + "请输入企业名称",
				maxlength : icon + "企业名称不能超过50个字符",
			},
			companyCode:{
				required: icon + "请输入企业代码",
				isCompanyCode: icon + "请填写正确的企业代码"
			},
			applyName : {
				required : icon + "请输入法人姓名",
				maxlength : icon + "法人姓名不能超过50个字符",
			},
			applyCardNumber: {
				required: icon + "请输入法人身份证号码",
				isIdentity: icon + "请输入正确的法人身份证号码",
			},
			mobile:{
				required: icon + "请输入手机号",
				isPhone: icon + "请填写正确的11位手机号"
			},
		}
	})
}
