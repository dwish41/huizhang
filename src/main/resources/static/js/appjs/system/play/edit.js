$().ready(function() {
    validateRule();

    $('.summernote').summernote({
        height : '220px',
        lang : 'zh-CN',
        callbacks: {
            onImageUpload: function(files, editor, $editable) {
                sendFile(files);
            }
        }
    });
});

$.validator.setDefaults({
    submitHandler : function() {
        update();
    }
});
function update() {
    //判断修改也是否上传图片，没有上传图片则把oldAttachId值放到新的attachId中去
    var attachId = $("#attachId").val();
    var flag = judeImage(attachId);
    if (!flag) {
        $("#attachId").val($("#oldAttachId").val());
    }
    $.ajax({
        cache: true,
        type: "POST",
        url: "/scenery/play/update",
        data: $('#signupForm').serialize(),// 你的formid
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);

            } else {
                parent.layer.alert(data.msg)
            }
        }
    });
}

function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            introduction : {
                required : true,
                maxlength: 50
            }
        },
        messages : {
            introduction : {
                required : icon + "请输入图片简介",
                maxlength: icon + "图片简介不能超过五十个字符",
            }
        }
    })
}





function judeImage(attachId) {
    var flag = false;
    $.ajax({
        type : "POST",
        url :  ctx + "/common/sysFile/list",
        data : {attachId:attachId},
        async : false,
        success : function(data) {
            if(data != null && data != '') {
                flag = true;
            }
        }
    });
    return flag;
}