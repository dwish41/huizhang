//实例化编辑器
var ue = UE.getEditor('container');
ue.addListener("ready", function () {
    var articleContent = $("#articleContent").val();
    ue.setContent(articleContent);
});

$().ready(function() {

    validateRule();
});

$.validator.setDefaults({
    submitHandler : function() {
        save();
    }
});

function closeIndex() {
    var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    parent.layer.close(index);
}

function save() {
    var content_sn = ue.getContent();
    if (content_sn == null || content_sn == '') {
        parent.layer.alert("请输入内容")
        return false;
    }
    $("#articleContent").val(content_sn);
    $.ajax({
        cache : true,
        type : "POST",
        url : "/scenery/article/save",
        data : $('#signupForm').serialize(),// 你的formid
        async : false,
        error : function(request) {
            parent.layer.alert("Connection error");
        },
        success : function(data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
            } else {
                parent.layer.alert(data.msg)
            }
        }
    });
}
function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            articleTitle : {
                required : true,
                maxlength: 100
            },
            articleContent : {
                required : true
            }
        },
        messages : {
            articleTitle : {
                required : icon + "请输入文章标题",
                maxlength: icon + "文章标题不能超过一百个字符",
            },
            articleTitle : {
                required : icon + "请输入文章内容"
            }
        }
    })
}

