$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/system/word/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			title : {
				required : true,
				maxlength: 100
			},
			content : {
				required : true,
				maxlength: 200
			}
		},
		messages : {
			title : {
				required : icon + "请输入留言标题",
				maxlength: icon + "留言标题不能超过100个字符",
			},
			content : {
				required : icon + "请输入留言内容",
				maxlength: icon + "留言内容不能超过100个字符",
			}
		}
	})
}
