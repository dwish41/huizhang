$().ready(function() {
	loadAgentList();
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});

function closeIndex() {
	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
	parent.layer.close(index);
}

function loadAgentList(){
	var html = "";
	$.ajax({
		url : '/sys/agent/loadAgentList?roleId=3',
		success : function(data) {
			//加载数据
			for (var i = 0; i < data.length; i++) {
				html += '<option value="' + data[i].userId + '">' + data[i].name + '</option>'
			}
			$(".chosen-select").append(html);
			$(".chosen-select").chosen({
				maxHeight : 200
			});
			//点击事件
			$('.chosen-select').on('change', function(e, params) {
				console.log(params.selected);
				var opt = {
					query : {
						type : params.selected,
					}
				}
				$('#exampleTable').bootstrapTable('refresh', opt);
			});
		}
	});
}

function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/sys/staff/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});
}

function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			name : {
				required : true,
				maxlength : 20
			},
			username : {
				required : true,
				minlength : 2,
				remote : {
					url : "/sys/user/exit", // 后台处理程序
					type : "post", // 数据发送方式
					dataType : "json", // 接受数据格式
					data : { // 要传递的数据
						username : function() {
							return $("#username").val();
						}
					}
				}
			},
            idCardNumber: {
                required:true,
                isIdentity: true
            },
            mobile:{
                required:true,
                isPhone:true
            },
            email:{
                required:true,
                email:true
            },
			topic : {
				required : "#newsletter:checked",
				minlength : 2
			},
			agree : "required"
		},
		messages : {
			name : {
				required : icon + "请输入姓名",
				maxlength : icon + "姓名不能超过20个字符",
			},
			username : {
				required : icon + "请输入您的用户名",
				minlength : icon + "用户名必须两个字符以上",
				remote : icon + "用户名已经存在"
			},
            idCardNumber: {
                required: icon + "请输入身份证号码",
                isIdentity: icon + "请输入正确的身份证号码",
            },
            mobile:{
                required: icon + "请输入手机号",
                isPhone: icon + "请填写正确的11位手机号"
            },
            email:{
                required: icon + "请输入邮箱",
                email: icon + "请填写正确的邮箱格式"
            }
		}
	})
}
