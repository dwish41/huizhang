var proviceAreaId = $("#proviceAreaId").val();
var cityAreaId = $("#cityAreaId").val();
var qxAreaId = $("#qxAreaId").val();
if(proviceAreaId != null && proviceAreaId != '' ) {
    getAreaInfoFor(1,proviceAreaId,2);
}else {
    getAreaInfo(1,2);
}
if(cityAreaId != null && cityAreaId != '' ) {
    getAreaInfoFor(proviceAreaId,cityAreaId,3);
}
if(qxAreaId != null && qxAreaId != '' ) {
    getAreaInfoFor(cityAreaId,qxAreaId,4);
}


function getAreaInfo(areaId,areaLevel) {
    if(areaId != '' && areaId != null) {
        $.ajax({//发送AJAX请求
            type : "POST",
            url : ctx + "/common/area/getareainfo?areaId="+areaId+"&areaLevel="+areaLevel ,
            success : function(msg) {
                if(msg != ''){
                    $('#area'+areaLevel).html(msg);
                    $('#area'+areaLevel).show();
                }else{
                    for(var i=areaLevel;i<=5;i++){
                        $('#area'+i).hide();
                    }
                }
            }
        });
    }else{
        //去掉多余(t+1)的分类
        var start= parseInt(areaLevel-1);
        if(start < 3) {
            start = 3;
        }
        for(var t=start;t<=5;t++){
            $("#area"+t).html("");
            $("#area"+t).css("display","none");
        }
    }
}

//地区取值
function setArea(val, classLevel, id){
    if(val != '' && val != null ){
        if(classLevel == 2) {
            $('#proviceAreaId').val(val);
            $('#cityAreaId').val("");
            $('#qxAreaId').val("");
        }else if(classLevel == 3) {
            $('#cityAreaId').val(val);
            $('#qxAreaId').val("");
        }else if(classLevel == 4) {
            $('#qxAreaId').val(val);
        }
    }else{
        var num=classLevel;
        if(num==0){
            val="";
        }else{
            val=$("#area"+(num-1)).val();
            for(var t=classLevel+1;t<=4;t++){ //去掉多余的项
                console.log("t=" + t);
                $("#area"+t).html("");
                $("#area"+t).css("display","none");
            }
        }
        if(classLevel == 2) {
            $('#proviceAreaId').val("");
            $('#cityAreaId').val("");
            $('#qxAreaId').val("");
        }
        if(classLevel == 3) {
            $('#cityAreaId').val("");
            $('#qxAreaId').val("");
        }
        if(classLevel == 4) {
            $('#qxAreaId').val("");
        }
    }
}


function getAreaInfoFor(parentId,areaId,areaLevel) {
    if(areaId != '' && areaId != null) {
        $.ajax({//发送AJAX请求
            type : "POST",
            url : ctx + "/common/area/getareainfofor?parentId="+parentId+"&areaId="+areaId+"&areaLevel="+areaLevel ,
            success : function(msg) {
                if(msg != ''){
                    $('#area'+areaLevel).html(msg);
                    $('#area'+areaLevel).show();
                }else{
                    for(var i=areaLevel;i<=5;i++){
                        $('#area'+i).hide();
                    }
                }
            }
        });
    }
}
