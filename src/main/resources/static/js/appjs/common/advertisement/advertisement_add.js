var prefix = ctx+ "/system/advertisement"
$(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		sbumitData();
	}
});

function closeIndex() {
	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
	parent.layer.close(index);
}

function sbumitData() {
	$.ajax({
		cache : true,
		type : "POST",
		url : prefix+ "/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}

function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			advContent : {
				required : true,
				maxlength:2000
			},
			advCode : {
				required : true,
				number : true
			},
			advName : {
				required : true,
				maxlength:50
			}
		},
		messages : {
			advName : {
				required : icon  + "轮播位名称不能为空",
				maxlength: icon  + "最大长度不超过50"
			},
			advCode : {
				required : icon  + "轮播序号不能为空",
                number : icon  + "轮播序号为纯数字",
			},
			advContent : {
				required : icon  + "轮播内容不能为空",
				maxlength: icon  + "最大长度不超过2000"
			}
		}
	})
}